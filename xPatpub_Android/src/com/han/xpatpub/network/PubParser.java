package com.han.xpatpub.network;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.feature.ActionMenu;
import com.han.xpatpub.model.Pub;
import com.han.xpatpub.model.Rate;

public class PubParser extends Parser {
	
	public static ArrayList<Pub> arrSearchPub = new ArrayList<Pub>();
	public static int nAddPubID;
	
	public static Integer parseSearchPub(String strResponse) {
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			JSONArray arrJsonPub = jsonObj.getJSONArray("record");
			ArrayList<Pub> arrPub = new ArrayList<Pub>();
			
			for (int i = 0; i < arrJsonPub.length(); i++) {
				
				JSONObject jsonPub = arrJsonPub.getJSONObject(i);
				Pub pub = new Pub();
				
				pub.id = jsonPub.getInt("pubId");
				pub.name = jsonPub.getString("pubName");
				pub.lat = jsonPub.getDouble("pubLat");
				pub.lng = jsonPub.getDouble("pubLong");
				pub.type = jsonPub.getInt("pubCategory");
				pub.iconUrl = jsonPub.getString("pubicon");
				pub.dis_min = Double.parseDouble(jsonPub.getString("distance_in_mi"));
				
				JSONObject jsonFeature = jsonPub.getJSONObject("pubfeature");
				
				pub.nVibe = Pub.getHighestNumber(jsonFeature.getString("Vibe"));
				Log.e("pub nVibe", Integer.toString(pub.id) + " : " + Integer.toString(pub.nVibe));
				pub.nTunes = Pub.getHighestNumber(jsonFeature.getString("Tunes"));
				pub.nAles = Pub.getHighestNumber(jsonFeature.getString("AlesonTap"));

				String strFeature = jsonFeature.getString("Action");
				String[] strActions = strFeature.split(",");
				
				for (int j = 0; j < strActions.length; j++) {
					if (strActions[j].equals(Integer.toString(ActionMenu.GAMES_ON_TV))) {
						pub.isGames = true;
					}
					if (strActions[j].equals(Integer.toString(ActionMenu.DARTS))) {
						pub.isDarts = true;
					}
					if (strActions[j].equals(Integer.toString(ActionMenu.POOL))) {
						pub.isPool = true;
					}
					if (strActions[j].equals(Integer.toString(ActionMenu.CARDS))) {
						pub.isCards = true;
					}
					if (strActions[j].equals(Integer.toString(ActionMenu.SHUFFLEBOARD))) {
						pub.isShuffleboard = true;
					}
				}
//				pub.avgRating = jsonObj.getString("avgRating");

//				JSONArray arrJsonRate = jsonPub.getJSONArray("pubRating");
//				
//				pub.arrRate = new ArrayList<Rate>();
//				double avgRate = 0;
//				for (int j = 0; j < arrJsonRate.length(); j++) {
//
//					JSONObject jsonRate = arrJsonRate.getJSONObject(j);
//					
//					Rate rate = new Rate();
//					rate.name = jsonRate.getString("rateCategoryName");
//					rate.avgRate = jsonRate.getDouble("avgRating");
//					avgRate += rate.avgRate;
//					pub.arrRate.add(rate);
//				}
				
//				pub.avgRating = (double) ((double) avgRate / (double) pub.arrRate.size());
				arrPub.add(pub);
			}
			
			arrSearchPub = arrPub;
			
			Log.e("pub count", Integer.toString(arrPub.size()));
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
	
	public static Integer parseAddPub(String strResponse) {
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			nAddPubID = jsonObj.getInt("pubId");
			Log.e("add pub id", Integer.toString(nAddPubID));
			
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
}
