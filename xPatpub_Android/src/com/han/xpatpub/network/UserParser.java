package com.han.xpatpub.network;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.feature.ActionMenu;
import com.han.xpatpub.model.Pub;
import com.han.xpatpub.model.User;

public class UserParser extends Parser {

//	public static ArrayList<User> arrSearchUser = new ArrayList<User>();
	
	public static Integer parseSearchUser(String strResponse) {
		
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			JSONArray arrJsonUser = jsonObj.getJSONArray("record");
			ArrayList<User> arrUser = new ArrayList<User>();
			
			for (int i = 0; i < arrJsonUser.length(); i++) {
				
				JSONObject jsonUser = arrJsonUser.getJSONObject(i);
				User user = new User();
				
				user.userID 		= jsonUser.getString("userId");
				user.userName 	= jsonUser.getString("userName");
				user.email 		= jsonUser.getString("userEmail");
				user.userLat 		= jsonUser.getString("userLat");
				user.userLng 		= jsonUser.getString("userLong");
				user.userLastLogin = jsonUser.getString("userLastLogin");
				user.userPrivacy = jsonUser.getString("userPrivacy");
				user.userType 	= jsonUser.getString("userType");

				arrUser.add(user);
			}
			
			GlobalData.arrSearchUser = arrUser;
			
			Log.e("user count", Integer.toString(arrUser.size()));
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
}
