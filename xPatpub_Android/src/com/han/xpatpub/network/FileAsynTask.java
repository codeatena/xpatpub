package com.han.xpatpub.network;

import java.io.File;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.LoginActivity;
import com.han.xpatpub.model.Advertise;
import com.han.xpatpub.model.Coupon;
import com.han.xpatpub.model.Pub;
import com.han.xpatpub.setting.CouponActivity;
import com.han.xpatpub.utility.DialogUtility;
import com.han.xpatpub.utility.TimeUtility;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

public class FileAsynTask extends AsyncTask<String, Integer, Integer> {
	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public ProgressDialog dlgLoading;
    public String curAction;
    
    public static String ACTION_UPDATE_PUB_ICON = "action_update_pub_icon";
    public static String ACTION_COUPON_LIST = "action_coupon_list";
    public static String ACTION_SEND_MESSAGE = "action_send_message";
    public static String ACTION_ADVERTISE_LIST = "action_advertise_list";
    public static String ACTION_GET_CURRENT_CITY = "action_get_current_city";
    
    public static String URL_UPDATE_PUB_ICON = "http://173.248.174.4:8080/rest/getGenericServices/?app_name=TestApp&action=updatePubIcon";
    public static String URL_COUPON_LIST = "http://173.248.174.4:8080/rest/xpatpub/tblcoupon?app_name=TestApp";
    public static String URL_SEND_MESSAGE = "http://173.248.174.4:8080/rest/xpatpub/tblmessage?app_name=TestApp";
    public static String URL_ADVERTISE_LIST = "http://173.248.174.4:8080/rest/getGenericServices/?app_name=TestApp&action=getNearByAds";
    public static String URL_GET_CURRENT_CITY = "http://xpatpub.com/api/json.php?action=getCity";
    
	public Context parent;
    public int nResult;
	
	public FileAsynTask(Context context) {
		parent = context;
	}
	
	protected Integer doInBackground(String... params) {
		// TODO Auto-generated method stub
		nResult = FAIL;
		
		curAction = params[0];
		if (curAction.equals(ACTION_UPDATE_PUB_ICON)) {
			String filePath = params[1];
			String pubID = params[2];
			nResult = updatePubIcon(filePath, pubID);
		}
		if (curAction.equals(ACTION_COUPON_LIST)) {
			nResult = readCouponList();
		}
		if (curAction.equals(ACTION_SEND_MESSAGE)) {
			String msgText = params[1];
			String msgRecieverID = params[2];
			String msgCouponID = params[3];
			nResult = sendMessage(msgText, msgRecieverID, msgCouponID);
		}
		if (curAction.equals(ACTION_ADVERTISE_LIST)) {
			getAdvertiseList();
		}
		if (curAction.equals(ACTION_GET_CURRENT_CITY)) {
			getCurrentCity();
		}
		
		return nResult;
	}
	
	@Override
	protected void onPreExecute() {
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage("\tLoading...");
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
        dlgLoading.show();
	}

	protected void onPostExecute(Integer result) {
		if (dlgLoading.isShowing())
			dlgLoading.dismiss();
		if (curAction.equals(ACTION_SEND_MESSAGE)) {
			CouponActivity couponActivity = (CouponActivity) parent;
			DialogUtility.showGeneralAlert(couponActivity, "Sent Coupon", "Your offer has been sent!");
		}
	}
	
	public Integer updatePubIcon(String filePath, String pubID) {
		try {
			String url = URL_UPDATE_PUB_ICON + "&pubId=" + pubID;
			
			Log.e("update pub icon url", url);
			url = "http://xpatpub.com/api/json.php?action=updatePubIcon&pubId=" + pubID;
			
			HttpPost httpPost = new HttpPost(url);
			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = null;
			
//			httpPost.setHeader("Accept", "application/json");
//			httpPost.setHeader("Content-type", "application/json");
			httpPost.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
//			Log.e("default_url", Environment.getExternalStorageState());
			Log.e("file_path", filePath);
			
            File inputFile = new File(filePath);
            if (inputFile.exists()) {
				MultipartEntity reqEntity = new MultipartEntity();
//                reqEntity.addPart("file_name", new StringBody("a.jpg"));
                reqEntity.addPart("pubIcon", new FileBody(inputFile));
                httpPost.setEntity(reqEntity);
                
    			response = httpClient.execute(httpPost);
    			
    			String strResponse = EntityUtils.toString(response.getEntity());
    			Log.e(curAction, strResponse);
            }

		} catch(Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
		return SUCCESS;
	}
	
	public Integer readCouponList() {
		try {
			
			String uri = Uri.parse(URL_COUPON_LIST)
	                .buildUpon()
	                .appendQueryParameter("filter", "couponuserId=" + GlobalData.currentUser.userID)
	                .build().toString();
			Log.e("GET Coupon List Url = ", uri);
			
			HttpGet httpGet = new HttpGet(uri);
			HttpClient httpClient = GetClient();
			HttpResponse response = null;

//	        httpGet.setEntity(se);
	        httpGet.setHeader("Accept", "application/json");
	        httpGet.setHeader("Content-type", "application/json");
	        httpGet.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
	        
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			
			return parseCouponList(strResponse);
		} catch (Exception e) {
	        Log.e("ClientProtocolException", e.toString());
	        return FAIL;
		}
	}
	
	public Integer parseCouponList(String jsonStr) {
		try {
			JSONObject jsonObj = new JSONObject(jsonStr);
			
			JSONArray arrJsonCoupon = jsonObj.getJSONArray("record");
			ArrayList<Coupon> arrCoupon = new ArrayList<Coupon>();
			  
			for (int i = 0; i < arrJsonCoupon.length(); i++) {
				
				Coupon coupon = new Coupon();
				
				JSONObject jsonCoupon = arrJsonCoupon.getJSONObject(i);
				
				coupon.id = jsonCoupon.getInt("couponId");
				coupon.couponType = jsonCoupon.getString("couponType");
				coupon.couponCode = jsonCoupon.getString("couponCode");
				coupon.couponDescription = jsonCoupon.getString("couponDescription");
				coupon.couponStartDate = jsonCoupon.getString("couponStartDate");
				coupon.couponExpireDate = jsonCoupon.getString("couponExpireDate");
				coupon.couponUsesLimit = jsonCoupon.getInt("couponUsesLimit");
				coupon.couponUserLimit = jsonCoupon.getInt("couponUserLimit");
				coupon.couponStatus = jsonCoupon.getInt("couponStatus");
				coupon.couponUserId = jsonCoupon.getInt("couponUserId");
				coupon.couponPubId = jsonCoupon.getInt("couponPubId");
				coupon.couponName = jsonCoupon.getString("couponName");
				
				arrCoupon.add(coupon);
			}
			
			GlobalData.arrMyCoupon = arrCoupon;
			
			Log.e("My Coupon Count", Integer.toString(GlobalData.arrMyCoupon.size()));
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
	
	public Integer sendMessage(String msgText, String msgRecieverID, String msgCouponID) {
		try {
			Log.e("Send Message Url", URL_SEND_MESSAGE);
			
			HttpPost httpPost = new HttpPost(URL_SEND_MESSAGE);
			HttpClient httpClient = GetClient();
			HttpResponse response = null;

            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("msgText", msgText);
            jsonObject.accumulate("msgCreatedDate", TimeUtility.getCurrentTime());
            jsonObject.accumulate("msgExpDate", TimeUtility.getDateAfter(20));
            jsonObject.accumulate("msgStatus", "0");
            jsonObject.accumulate("msgSenderId", GlobalData.currentUser.userID);
            jsonObject.accumulate("msgReceiverId", "1");
            jsonObject.accumulate("msgPubId", GlobalData.ownPub.id);
            jsonObject.accumulate("msgCouponId", msgCouponID);

            String json = jsonObject.toString();
            StringEntity se = new StringEntity(json);
 
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpPost);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			return SUCCESS;
			
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
	
	public Integer getAdvertiseList() {
		try {
			
			String url = URL_ADVERTISE_LIST + "&Lat=" + Double.toString(GlobalData.curLat) + "&Long=" + Double.toString(GlobalData.curLng) + 
					"&Radius=3.3" + "&session_id=" + GlobalData.currentUser.session_id;
			
//			String url = URL_ADVERTISE_LIST + "&Lat=44.853743&Long=-93.432202&Radius=3.3" + "&session_id=" + GlobalData.currentUser.session_id; 
			
			Log.e("GET Advertise List Url = ", url);
			
			HttpGet httpGet = new HttpGet(url);
			HttpClient httpClient = GetClient();
			HttpResponse response = null;

//	        httpGet.setEntity(se);
	        httpGet.setHeader("Accept", "application/json");
	        httpGet.setHeader("Content-type", "application/json");
	        httpGet.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
	        
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			
			return parseAdvertiseList(strResponse);
		} catch (Exception e) {
	        Log.e("ClientProtocolException", e.toString());
	        return FAIL;
		}
	}
	
	public Integer parseAdvertiseList(String jsonStr) {
		try {
			JSONObject jsonObj = new JSONObject(jsonStr);
			
			JSONArray arrJsonAdvertise = jsonObj.getJSONArray("record");
			ArrayList<Advertise> arrAdvertise = new ArrayList<Advertise>();
			  
			for (int i = 0; i < arrJsonAdvertise.length(); i++) {
				
				Advertise advertise = new Advertise();
				
				JSONObject jsonAdvertise = arrJsonAdvertise.getJSONObject(i);
				
				advertise.id = jsonAdvertise.getInt("adId");
				advertise.adIcon = jsonAdvertise.getString("adIcon");
				advertise.time = jsonAdvertise.getString("adTime");
				advertise.lat = jsonAdvertise.getDouble("adLat");
				advertise.lng = jsonAdvertise.getDouble("adLong");
				advertise.title = jsonAdvertise.getString("adTitle");
				advertise.type = jsonAdvertise.getString("adType");
				advertise.link = jsonAdvertise.getString("adLink");
				
				arrAdvertise.add(advertise);
			}
			
			GlobalData.arrAdvertise = arrAdvertise;
			
			Log.e("My Advertise Count", Integer.toString(GlobalData.arrAdvertise.size()));
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
	
	public Integer getCurrentCity() {
		try {
			
			String url = URL_GET_CURRENT_CITY + "&Lat=" + GlobalData.curLat + "&Long=" + GlobalData.curLng; 
			
			Log.e("GET current city Url = ", url);
			
			HttpGet httpGet = new HttpGet(url);
			HttpClient httpClient = GetClient();
			HttpResponse response = null;
	        
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			
			GlobalData.curCityName = strResponse;
			
			return SUCCESS;
		} catch (Exception e) {
	        Log.e("ClientProtocolException", e.toString());
	        return FAIL;
		}
	}
	
	public static DefaultHttpClient GetClient() {
		DefaultHttpClient ret = null;

		// sets up parameters
		HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params, "utf-8");
		params.setBooleanParameter("http.protocol.expect-continue", false);

		// registers schemes for both http and https
		SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		registry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
		
		ThreadSafeClientConnManager manager = new ThreadSafeClientConnManager(params, registry);
		ret = new DefaultHttpClient(manager, params);
		
		return ret;
	}
}
