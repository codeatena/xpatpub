package com.han.xpatpub.network;

import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.LoginActivity;
import com.han.xpatpub.model.Pub;
import com.han.xpatpub.model.Rate;
import com.han.xpatpub.pub.AddPubActivity;
import com.han.xpatpub.pub.CritiqueActivity;
import com.han.xpatpub.pub.FeatureActivity;
import com.han.xpatpub.pub.ResultActivity;
import com.han.xpatpub.setting.MessageActivity;

public class MyAsyncTask extends AsyncTask<String, Integer, Integer> {
	
	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public static final int PUB_ALEADY_EXIST = 2;

    public Context parent;
    public ProgressDialog dlgLoading;
    
    public String curAction;
    public int nResult;
    
    public static final String ACTION_SESSION = "action_session";
    public static final String ACTION_SEARCH_PUB = "action_search_pub";
    public static final String ACTION_PUB_DETAIL = "action_pub_detail";
    public static final String ACTION_ADD_PUB = "action_add_pub";
    public static final String ACTION_ADD_RATE = "action_add_rate";
    public static final String ACTION_GET_COUNTRY_CODE = "action_get_country_code";
    public static final String ACTION_GET_COUNTRY_CODE_FROM_GEO_DATA = "action_get_country_code_from_geo_data";
    public static final String ACTION_PUB_OWNER_DETAIL = "action_pub_owner_detail";
    public static final String ACTION_GET_CURRENT_CITY = "action_get_current_city";
    public static final String ACTION_UPDATE_FEATURE = "action_update_feaure";
    public static final String ACTION_USER_LIST = "action_user_list";
    public static final String ACTION_CREATE_MESSAGE = "action_create_message";
    public static final String ACTION_MARK_MESSAGE = "action_mark_message";
	public static final String ACTION_USER_MESSAGE = "action_my_message";
    
    public static final String URL_SESSION = "http://173.248.174.4:8080/rest/user/session?app_name=TestApp";
    public static final String URL_CUSTOM = "http://173.248.174.4:8080/rest/getGenericServices/?app_name=TestApp";
    public static final String URL_ADD_PUB = "http://173.248.174.4:8080/rest/xpatpub/tblpubs/?app_name=TestApp";
    public static final String URL_ADD_RATE = "http://173.248.174.4:8080/rest/xpatpub/tblrating/?app_name=TestApp";
    public static final String URL_GET_COUNTRY_CODE = "http://173.248.174.4:8080/rest/xpatpub/tblcountries?app_name=TestApp&filter=countryCode%3D";
    public static final String URL_PUB_DETAIL = "http://173.248.174.4:8080/rest/xpatpub/tblpubs?app_name=TestApp&include_count=true";
//    public static final String URL_DETAIL = "http://173.248.174.4:8080/rest/geodb/tblpubs/1/?app_name=TestApp";
    
	public MyAsyncTask(Context _parent) {
		parent = _parent;
	}
	
	@Override
	protected Integer doInBackground(String... params) {
		// TODO Auto-generated method stub
		nResult = FAIL;
		
		curAction = params[0];
		
		if (curAction.equals(ACTION_SESSION)) {
			String email = params[1];
			String password = params[2];
			nResult = session(email, password);
		}
		if (curAction.equals(ACTION_SEARCH_PUB)) {
			
			String strLat = params[1];
			String strLng = params[2];
			String strPubType = params[3];
			String strPubFeature = params[4];
			String strRadius = params[5];
			
			nResult = PubWebService.searhPub(strLat, strLng, strPubType, strPubFeature, strRadius);
			if (nResult == SUCCESS) {
				GlobalData.arrAllPub = PubParser.arrSearchPub;
			}
//			nResult = search(strLat, strLng, strPubFeature, strPubType, strSearchType);
		}
		if (curAction.equals(ACTION_PUB_DETAIL)) {
			String strID = params[1];
			nResult = pubDetail(strID);
		}
		if (curAction.equals(ACTION_PUB_OWNER_DETAIL)) {
			nResult = pubOwnerDetail();
		}
		if (curAction.equals(ACTION_ADD_PUB)) {
			
			String strPubName = params[1];
			String strPubType = params[2];
			String strLat = params[3];
			String strLng = params[4];
			String strPubCountry = params[5];
			String strPubCategory = params[6];
			String strPubIcon = params[7];
			String strPubFounder = params[8];
			String strPubFeature = params[9];
			
			nResult = PubWebService.searhPub(strLat, strLng, "", "", Double.toString(GlobalData.MILES_FOR_METER_10));
			
			if (nResult == SUCCESS) {
				int nCount = PubParser.arrSearchPub.size();
				if (nCount > 0) {
					nResult = PUB_ALEADY_EXIST;
				} else {
					nResult = PubWebService.addPub(strPubName, strPubType, strLat, strLng, strPubCountry, strPubCategory, strPubIcon, strPubFounder);
					if (nResult == SUCCESS) {
						int nAddPubId = PubParser.nAddPubID;
						
						String[] arrStr = strPubFeature.split(",");
						
						for (int i = 0; i < arrStr.length; i++) {
							nResult = PubWebService.addPubFeature(nAddPubId, Integer.parseInt(arrStr[i]));
							if (nResult == FAIL) {
								return nResult;
							}
						}
					}
				}
			}
//			nResult = addPub(strName, strType, strIsHaveLiveMusic, strIsHaveTVSports, strIsHavePubGames);
		}
		if (curAction.equals(ACTION_ADD_RATE)) {
			
			String strPubID = params[1];
//			String strRateOptionID = params[2];
//			String strRate = params[3];
			String[] strRate = new String[5];
			
			strRate[0] = params[2];
			strRate[1] = params[3];
			strRate[2] = params[4];
			strRate[3] = params[5];
			strRate[4] = params[6];
			
			nResult = addRate(strPubID, strRate);
		}
		if (curAction.equals(ACTION_GET_COUNTRY_CODE)) {
			nResult = setCountryCode();
		}
		if (curAction.equals(ACTION_GET_COUNTRY_CODE_FROM_GEO_DATA)) {
			String strLat = params[1];
			String strLng = params[2];
			nResult = setCountryCodeFromGeoData(strLat, strLng);
		}
		if (curAction.equals(ACTION_GET_CURRENT_CITY)) {
//			nResult = setCurrentCity();
		}
		if (curAction.equals(ACTION_UPDATE_FEATURE)) {
			int nPubID = Integer.parseInt(params[1]);
//			nResult = PubWebService.deletePubFeaure(nPubID);
			nResult = SUCCESS;

			String strPubFeature = params[2];
			
			if (nResult == SUCCESS) {
				String[] arrStr = strPubFeature.split(",");
				
				for (int i = 0; i < arrStr.length; i++) {
					nResult = PubWebService.addPubFeature(nPubID, Integer.parseInt(arrStr[i]));
					if (nResult == FAIL) {
						return nResult;
					}
				}
			}
		}
		if (curAction.equals(ACTION_USER_LIST)) {
			String strLat = params[1];
			String strLng = params[2];
			String strRadius = params[3];
			
			nResult = UserWebService.searchUser(strLat, strLng, strRadius);
		}
		if (curAction.equals(ACTION_CREATE_MESSAGE)) {
			String msgText = "test msg1";
			String msgStatus = "0";
			String msgSenderId = params[1];
			String msgReceiverId = params[2];
			String msgPubId = params[3];
			String msgCouponId = params[4];

			nResult = MessageWebService.addMessage(msgText, msgStatus, msgSenderId, msgReceiverId, msgPubId, msgCouponId);
			if (nResult == SUCCESS) {
				nResult = MessageWebService.readMyMessage();
			}
		}
		if (curAction.equals(ACTION_MARK_MESSAGE)) {
			String msgId = params[1];
			nResult = MessageWebService.markMessage(msgId);
			if (nResult == SUCCESS) {
				nResult = MessageWebService.readMyMessage();
			}
		}
		if (curAction.equals(ACTION_USER_MESSAGE)) {
			nResult = MessageWebService.readMyMessage();
		}
		
		return nResult;
	}
	
	@Override
	protected void onPreExecute() {
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage("\tLoading...");
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
        dlgLoading.show();
	}

	protected void onPostExecute(Integer result){
		
		if (dlgLoading.isShowing())
			dlgLoading.dismiss();
//		parent.startActivity(new Intent(parent, CompleteActivity.class));
		
		if (curAction == ACTION_SESSION) {
			LoginActivity loginActivity = (LoginActivity) parent;
			loginActivity.successSession();
		}
		if (curAction == ACTION_SEARCH_PUB) {
			if (nResult == SUCCESS) {
				//SearchActivity searchActivity = (SearchActivity) parent;
				//searchActivity.drawPubList();
				if (parent instanceof FeatureActivity) {
					FeatureActivity featureActivity = (FeatureActivity) parent;
					featureActivity.sucessSearchPub();
				}
				if (parent instanceof CritiqueActivity) {
					CritiqueActivity critiqueActivity = (CritiqueActivity) parent;
					critiqueActivity.successSearch();
				}
				if (parent instanceof  AddPubActivity) {
					AddPubActivity addPubActivity = (AddPubActivity) parent;
					addPubActivity.successSearch();
				}
			}
		}
		if (curAction == ACTION_PUB_DETAIL) {
			if (nResult == SUCCESS) {

			}
		}
		if (curAction == ACTION_PUB_OWNER_DETAIL) {
			if (nResult == SUCCESS) {
				
			}
		}
		if (curAction == ACTION_ADD_PUB) {
			if (nResult == SUCCESS) {
				AddPubActivity addPubActivity = (AddPubActivity) parent;
				addPubActivity.successSubmit();
			}
			if (nResult == PUB_ALEADY_EXIST) {
				AddPubActivity addPubActivity = (AddPubActivity) parent;
				addPubActivity.pubExistAleady();
			}
		}
		if (curAction == ACTION_ADD_RATE) {
			if (nResult == SUCCESS) {
				CritiqueActivity critiqueActivity = (CritiqueActivity) parent;
				critiqueActivity.successAddRate();
			}
		}
		if (curAction == ACTION_UPDATE_FEATURE) {
			if (nResult == SUCCESS) {
				CritiqueActivity critiqueActivity = (CritiqueActivity) parent;
				critiqueActivity.successUpdateFeature();
			}
		}
		if (curAction.equals(ACTION_USER_MESSAGE)) {
			if (nResult == SUCCESS) {
				ResultActivity.setUnreadMessageCount();
			}
		}
		if (curAction.equals(ACTION_MARK_MESSAGE)) {
			if (nResult == SUCCESS) {
				ResultActivity.setUnreadMessageCount();
				MessageActivity messageActivity = (MessageActivity) parent;
				messageActivity.successMarkMessage();
			}
		}
		if (curAction.equals(ACTION_CREATE_MESSAGE)) {
			if (nResult == SUCCESS) {
				ResultActivity.setUnreadMessageCount();
			}
		}
	}
	
	protected void onProgressUpdate(Integer... progress) {
		
	}
	
	public Integer session(String email, String password) {
		
		try {
			HttpPost httpPost = new HttpPost(URL_SESSION);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
			 
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("email", email);
            jsonObject.accumulate("password", password);
 
            String json = jsonObject.toString();
            StringEntity se = new StringEntity(json);
 
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
			
			response = httpClient.execute(httpPost);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			return parseSession(strResponse);
			
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}		
	}
	
	public Integer parseSession(String jsonStr) {
		try {
			JSONObject jsonObj = new JSONObject(jsonStr);
//			GlobalData.currentUser.firstName = jsonObj.getString("first_name");
//			GlobalData.currentUser.lastName = jsonObj.getString("last_name");
			GlobalData.currentUser.session_id = jsonObj.getString("session_id");
//			GlobalData.currentUser.email = jsonObj.getString("email");
			
			Log.e("session_id", GlobalData.currentUser.session_id);
//			Log.e("FirstName and Last Name", GlobalData.currentUser.firstName + " " + GlobalData.currentUser.lastName);
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
		
		return SUCCESS;
	}
	
	public Integer search(String strLat, String strLng, String strPubFeature, String strPubType, String strSearchType) {
		
		try {
			String url = URL_CUSTOM + "&action=getNearByPubs" +"&Lat=" + strLat + "&Long=" + strLng + "&session_id=" + GlobalData.currentUser.session_id;
			
			if (strSearchType.equals(GlobalData.SEARCH_TYPE_NONE)) {
				url += "&Radius=0.4";
			} else if (strSearchType.equals(GlobalData.SEARCH_TYPE_WALK_ONLY)) {
				url += "&Radius=0.4";
			} else if (strSearchType.equals(GlobalData.SEARCH_TYPE_CAB_IT)) {
				url += "&Radius=3.3";
			}
			
			if (!strPubFeature.equals("")) {
				url += "&pubfeature=" + strPubFeature;
			}
			if (!strPubType.equals("")) {
				url += "&pubType=" + strPubType;
			}
			
			Log.e("Url = ", url);
			HttpGet httpGet = new HttpGet(url);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
 
//            httpGet.setEntity(se);
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			
			return parseSearch(strResponse);
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
	
	public Integer parseSearch(String jsonStr) {
		
		try {
			JSONObject jsonObj = new JSONObject(jsonStr);
			
			JSONArray arrJsonPub = jsonObj.getJSONArray("record");
			ArrayList<Pub> arrPub = new ArrayList<Pub>();
			
			for (int i = 0; i < arrJsonPub.length(); i++) {
				
				JSONObject jsonPub = arrJsonPub.getJSONObject(i);
				Pub pub = new Pub();
				
				pub.id = jsonPub.getInt("pubId");
				pub.name = jsonPub.getString("pubName");
				pub.lat = jsonPub.getDouble("pubLat");
				pub.lng = jsonPub.getDouble("pubLong");
				pub.type = jsonPub.getInt("pubCategory");
//				pub.type = jsonPub.getInt("");
				
				pub.isHaveLiveMusic = (jsonPub.getInt("pubHaveLiveMusic") == 1) ? true : false;
				pub.isHaveTVSports = (jsonPub.getInt("pubHaveTVSports") == 1) ? true : false;
				pub.isHavePubGames = (jsonPub.getInt("pubHavePoolDarts") == 1) ? true : false;
				pub.dis_min = Double.parseDouble(jsonPub.getString("distance_in_mi"));
				
//				pub.avgRating = jsonObj.getString("avgRating");

				JSONArray arrJsonRate = jsonPub.getJSONArray("pubRating");
				
				pub.arrRate = new ArrayList<Rate>();
				double avgRate = 0;
				for (int j = 0; j < arrJsonRate.length(); j++) {

					JSONObject jsonRate = arrJsonRate.getJSONObject(j);
					
					Rate rate = new Rate();
					rate.name = jsonRate.getString("rateCategoryName");
					rate.avgRate = jsonRate.getDouble("avgRating");
					avgRate += rate.avgRate;
					pub.arrRate.add(rate);
				}
				
				pub.avgRating = (double) ((double) avgRate / (double) pub.arrRate.size());
				arrPub.add(pub);
			}
			
			GlobalData.arrAllPub = arrPub;
			
			Log.e("pub count", Integer.toString(arrPub.size()));
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
	
	public Integer pubOwnerDetail() {
		try {
			String url = URL_PUB_DETAIL + "&filter=pubOwner=" + GlobalData.currentUser.userID;
			
			Log.e("pub owner url = ", url);
			HttpGet httpGet = new HttpGet(url);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
 
//            httpGet.setEntity(se);
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			
			return parsePubOwnerDetail(strResponse);
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
	
	public Integer parsePubOwnerDetail(String jsonStr) {
		try {
			JSONObject jsonObj = new JSONObject(jsonStr);
			JSONArray arrJsonPub = jsonObj.getJSONArray("record");
			Pub pub = new Pub();
			
			JSONObject jsonPub = arrJsonPub.getJSONObject(0);

			pub.id = jsonPub.getInt("pubId");
			pub.name = jsonPub.getString("pubName");
			pub.type = jsonPub.getInt("pubType");
			pub.lat = jsonPub.getDouble("pubLat");
			pub.lng = jsonPub.getDouble("pubLong");
			pub.country = jsonPub.getInt("pubCountry");
			pub.category = jsonPub.getInt("pubCategory");
			
			pub.isHaveLiveMusic = (jsonPub.getInt("pubHaveLiveMusic") == 1) ? true : false;
			pub.isHaveTVSports = (jsonPub.getInt("pubHaveTVSports") == 1) ? true : false;
			pub.isHavePubGames = (jsonPub.getInt("pubHavePoolDarts") == 1) ? true : false;
			
			pub.iconUrl = jsonPub.getString("pubIcon");
			pub.recommondYes = jsonPub.getInt("pubRecommondYes");
			pub.recommondNo = jsonPub.getInt("pubRecommondNo");
			pub.totalView = jsonPub.getInt("pubTotalViews");
			pub.founder = jsonPub.getInt("pubFounder");
			pub.owner = jsonPub.getInt("pubOwner");
			
			GlobalData.ownPub = pub;
			
			Log.e("Own Pub name,  id", pub.name + ",   " + Integer.toString(pub.id));
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
	
	public Integer pubDetail(String strID) {
		try {
			String url = URL_PUB_DETAIL + "&filter=pubId=" + strID;
			
			Log.e("Url = ", url);
			HttpGet httpGet = new HttpGet(url);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
 
//            httpGet.setEntity(se);
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			
			return parsePubDetail(strResponse);
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
	
	public Integer parsePubDetail(String jsonStr) {
		try {
			JSONObject jsonObj = new JSONObject(jsonStr);
			JSONArray arrJsonPub = jsonObj.getJSONArray("record");
			Pub pub = new Pub();
			
			JSONObject jsonPub = arrJsonPub.getJSONObject(0);

			pub.id = jsonPub.getInt("pubId");
			pub.name = jsonPub.getString("pubName");
			pub.type = jsonPub.getInt("pubType");
			pub.lat = jsonPub.getDouble("pubLat");
			pub.lng = jsonPub.getDouble("pubLong");
			pub.country = jsonPub.getInt("pubCountry");
			pub.category = jsonPub.getInt("pubCategory");
			
			pub.isHaveLiveMusic = (jsonPub.getInt("pubHaveLiveMusic") == 1) ? true : false;
			pub.isHaveTVSports = (jsonPub.getInt("pubHaveTVSports") == 1) ? true : false;
			pub.isHavePubGames = (jsonPub.getInt("pubHavePoolDarts") == 1) ? true : false;
			
			pub.iconUrl = jsonPub.getString("pubIcon");
			pub.recommondYes = jsonPub.getInt("pubRecommondYes");
			pub.recommondNo = jsonPub.getInt("pubRecommondNo");
			pub.totalView = jsonPub.getInt("pubTotalViews");
			pub.founder = jsonPub.getInt("pubFounder");
			pub.owner = jsonPub.getInt("pubOwner");
			
			if (GlobalData.getPub(pub.id) == null) 
				GlobalData.arrAllPub.add(pub);
			
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
	
	public Integer addPub(String strName, String strType, String strIsHaveLiveMusic, String strIsHaveTVSports, 
			String strIsHavePubGames) {

		try {
			HttpPost httpPost = new HttpPost(URL_ADD_PUB);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
			 
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("pubName", strName);
            jsonObject.accumulate("pubType", strType);
            jsonObject.accumulate("pubLat", Double.toString(GlobalData.curLat));
            jsonObject.accumulate("pubLong", Double.toString(GlobalData.curLng));
            jsonObject.accumulate("pubCountry", GlobalData.countryNumber);
            jsonObject.accumulate("pubCategory", strType);
            jsonObject.accumulate("pubHaveLiveMusic", strIsHaveLiveMusic);
            jsonObject.accumulate("pubHaveTVSports", strIsHaveTVSports);
            jsonObject.accumulate("pubHavePoolDarts", strIsHavePubGames);
            jsonObject.accumulate("session_id", GlobalData.currentUser.session_id);
//            jsonObject.accumulate("pubIcon", "3");
            jsonObject.accumulate("pubFounder", GlobalData.currentUser.userID);

            String json = jsonObject.toString();
            StringEntity se = new StringEntity(json);
 
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpPost);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			return SUCCESS;
			
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}	
	}
	
	public Integer addRate(String strPubID, String[] strRate) {
		try {
			
//			for (int i = 0; i < 5; i++) {
            JSONArray jsonArray = new JSONArray();

			for (int i = 0; i < 5; i++) {
				if (strRate[i].equals("None")) continue;
				
				JSONObject jsonRate = new JSONObject();
				
				jsonRate.put("ratePubId", strPubID);
				jsonRate.put("rateTime", GlobalData.getCurrentTime());
				jsonRate.put("rateOptionsId", i + 1);
				jsonRate.put("rateValues", strRate[i]);
				
				jsonArray.put(jsonRate);
			}
			
			HttpPost httpPost = new HttpPost(URL_ADD_RATE);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
			
			JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("session_id", GlobalData.currentUser.session_id);
            
            jsonObject.accumulate("record", jsonArray);

            String json = jsonObject.toString();
            
            Log.e("Add Rate JSON format", json);
            StringEntity se = new StringEntity(json);
            
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpPost);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
//			}
			
            return SUCCESS;
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}	
	}
		
	public Integer setCountryCode() {
		try {
			
			String url = URL_GET_COUNTRY_CODE + "'" + GlobalData.isoCountryCode + "'";
			Log.e("GET COUNTRY CODE Url = ", url);
			HttpGet httpGet = new HttpGet(url);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
 
//            httpGet.setEntity(se);
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			
			return parseCountryCode(strResponse);
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
	
	public Integer parseCountryCode(String jsonStr) {
		try {
			JSONObject jsonObj = new JSONObject(jsonStr);
			
			JSONArray arrJsonCountry = jsonObj.getJSONArray("record");
			JSONObject jsonCountry = arrJsonCountry.getJSONObject(0);
			
			GlobalData.countryNumber = jsonCountry.getString("countryId");
			
			Log.e("countryNumber", GlobalData.countryNumber);
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
	
	public Integer setCountryCodeFromGeoData(String strLat, String strLng) {
		try {
			String url = URL_CUSTOM + "&action=getGeoData" +"&Lat=" + strLat + "&Long=" + strLng + "&session_id=" + GlobalData.currentUser.session_id;
			
			Log.e("Country Code Url = ", url);
			HttpGet httpGet = new HttpGet(url);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
 
//            httpGet.setEntity(se);
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			GlobalData.countryNumber = strResponse;
			
			return SUCCESS;
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
}