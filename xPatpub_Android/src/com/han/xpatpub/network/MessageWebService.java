package com.han.xpatpub.network;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.net.Uri;
import android.util.Log;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.utility.TimeUtility;

public class MessageWebService extends WebService {
	
	public static String URL_MESSAGE = "http://173.248.174.4:8080/rest/xpatpub/tblmessage?app_name=TestApp";
	public static final String URL_READ_MESSAGE = "http://173.248.174.4:8080/rest/xpatpub/tblmessage?app_name=TestApp&include_count=true&msgStatus=0";

	public static Integer addMessage(String msgText, String msgStatus, String msgSenderId, String msgReceiverId, String msgPubId, String msgCouponId) {
		
		try {
			HttpPost httpPost = new HttpPost(URL_MESSAGE);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
			
            JSONObject jsonObject = new JSONObject();
            
            jsonObject.accumulate("msgText", msgText);
            jsonObject.accumulate("msgCreatedDate", TimeUtility.getCurrentTime());
            jsonObject.accumulate("msgEXPDate", TimeUtility.getCurrentTime());
            jsonObject.accumulate("msgStatus", "0");
            jsonObject.accumulate("msgSenderId", msgSenderId);
            jsonObject.accumulate("msgReceiverId", msgReceiverId);
            jsonObject.accumulate("msgPubId", msgPubId);
            jsonObject.accumulate("msgCouponId", msgCouponId);
            jsonObject.accumulate("msgCouponUsed", "0");

            String json = jsonObject.toString();
            StringEntity se = new StringEntity(json);
 
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpPost);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e("add message response", strResponse);
			
			return SUCCESS;
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
	
	public static Integer readMyMessage() {
		try {
//			String url = URL_LOGIN + "&filter=username='" + strUserName + "'";
			String uri = Uri.parse(URL_READ_MESSAGE)
                .buildUpon()
                .appendQueryParameter("filter", "msgReceiverId=" + GlobalData.currentUser.userID)
                .appendQueryParameter("order", "msgCreatedDate desc")
                .appendQueryParameter("related", "tblpubs_by_msgPubId,tblcoupon_by_msgCouponId")
                .build().toString();
			
			Log.e("GET MY Message Url = ", uri);
			HttpGet httpGet = new HttpGet(uri);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
 
//            httpGet.setEntity(se);
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e("read my message response", strResponse);
			
			return MessageParser.parseMyMessages(strResponse);
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
	
	public static Integer markMessage(String msgId) {
		
		try {
			HttpPatch httpPatch = new HttpPatch(URL_MESSAGE);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;


			JSONObject jsonObject = new JSONObject();
			JSONObject jsonRecord = new JSONObject();
			jsonRecord.accumulate("msgId", msgId);
			jsonRecord.accumulate("msgStatus", 1);
			jsonRecord.accumulate("msgCouponUsed", 1);
			jsonRecord.accumulate("msgUsedTime", TimeUtility.getCurrentTime());
			jsonObject.accumulate("record", jsonRecord);

            String json = jsonRecord.toString();
            StringEntity se = new StringEntity(json);
 
            httpPatch.setEntity(se);
            httpPatch.setHeader("Accept", "application/json");
            httpPatch.setHeader("Content-type", "application/json");
            httpPatch.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpPatch);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e("mark message response", strResponse);
			
			return SUCCESS;
			
		} catch (Exception e) {
			Log.e("ClientProtocolException", e.toString());
			return FAIL;
		}
	}
}
