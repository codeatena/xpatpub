package com.han.xpatpub.network;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

import android.util.Log;

import com.han.xpatpub.GlobalData;

public class UserWebService extends WebService{
	
    public static final String URL_CUSTOM = "http://173.248.174.4:8080/rest/getGenericServices/?app_name=TestApp";

	public static Integer searchUser(String strLat, String strLng, String strRadius) {
		try {
			String url = URL_CUSTOM + "&action=getNearUsersByPubs" +"&Lat=" + strLat + "&Long=" + strLng/* + "&session_id=" + GlobalData.currentUser.session_id*/;
			
			if (!strRadius.equals("")) {
				url += ("&Radius=" + strRadius);
			}
			
			Log.e("Url = ", url);
			HttpGet httpGet = new HttpGet(url);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
	
	//        httpGet.setEntity(se);
	        httpGet.setHeader("Accept", "application/json");
	        httpGet.setHeader("Content-type", "application/json");
	        httpGet.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
	        
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			
			Log.e("search user response", strResponse);
			return UserParser.parseSearchUser(strResponse);	
		} catch (Exception e) {
	        Log.e("ClientProtocolException", e.toString());
	        return FAIL;
		}
	}
}