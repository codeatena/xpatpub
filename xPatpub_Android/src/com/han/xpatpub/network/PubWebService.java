package com.han.xpatpub.network;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.net.Uri;
import android.util.Log;

import com.han.xpatpub.GlobalData;

public class PubWebService extends WebService {

    public static final String URL_CUSTOM = "http://173.248.174.4:8080/rest/getGenericServices/?app_name=TestApp";
    public static final String URL_PUB = "http://173.248.174.4:8080/rest/xpatpub/tblpubs/?app_name=TestApp";
    public static final String URL_PUB_FEATURE = "http://173.248.174.4:8080/rest/xpatpub/tblpubfeatures/?app_name=TestApp";

	public static Integer searhPub(String strLat, String strLng, String strPubType, String strPubFeature, String strRadius) {
		
		try {
			String url = URL_CUSTOM + "&action=getNearByPubs" +"&Lat=" + strLat + "&Long=" + strLng + "&session_id=" + GlobalData.currentUser.session_id;
			
			if (!strRadius.equals("")) {
				url += ("&Radius=" + strRadius);
			}
			if (!strPubFeature.equals("")) {
				url += "&pubfeature=" + strPubFeature;
			}
			if (!strPubType.equals("")) {
				url += "&pubType=" + strPubType;
			}
			
			Log.e("Url = ", url);
			HttpGet httpGet = new HttpGet(url);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
	
	//        httpGet.setEntity(se);
	        httpGet.setHeader("Accept", "application/json");
	        httpGet.setHeader("Content-type", "application/json");
	        httpGet.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
	        
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e("search pub response", strResponse);
			
			return PubParser.parseSearchPub(strResponse);
		} catch (Exception e) {
	        Log.e("ClientProtocolException", e.toString());
	        return FAIL;
		}
	}
	
	public static Integer addPub(String strPubName, String strPubType, String strLat, String strLng, String strPubCountry, 
			String strPubCategory, String strPubIcon, String strPubFounder) {

		try {
			HttpPost httpPost = new HttpPost(URL_PUB);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
			 
            JSONObject jsonObject = new JSONObject();
            
            jsonObject.accumulate("pubName", strPubName);
            jsonObject.accumulate("pubType", strPubType);
            jsonObject.accumulate("pubLat", strLat);
            jsonObject.accumulate("pubLong", strLng);
            jsonObject.accumulate("pubCountry", strPubCountry);
            jsonObject.accumulate("pubCategory", strPubCategory);
            jsonObject.accumulate("pubIcon", strPubIcon);
            jsonObject.accumulate("pubFounder", Integer.parseInt(strPubFounder));
            jsonObject.accumulate("session_id", GlobalData.currentUser.session_id);

            String json = jsonObject.toString();
            StringEntity se = new StringEntity(json);
 
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpPost);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e("add pub response", strResponse);
			
			return PubParser.parseAddPub(strResponse);
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
	
	public static Integer addPubFeature(int pubID, int featureID) {
		try {
			HttpPost httpPost = new HttpPost(URL_PUB_FEATURE);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;
			 
            JSONObject jsonObject = new JSONObject();
            
            jsonObject.accumulate("pubId", Integer.toString(pubID));
            jsonObject.accumulate("featureId", Integer.toString(featureID));
            jsonObject.accumulate("session_id", GlobalData.currentUser.session_id);

            String json = jsonObject.toString();
            StringEntity se = new StringEntity(json);
 
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpPost);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e("add pub feature response", strResponse);
			
			return SUCCESS;
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
	
	public static Integer deletePubFeaure(int pubID) {
		try {
			String uri = Uri.parse(URL_PUB_FEATURE)
	                .buildUpon()
	                .appendQueryParameter("filter", "pubId=" + Integer.toString(pubID))
	                .build().toString();
			Log.e("delete pub feature Url = ", uri);
			
			HttpDelete httpDelete = new HttpDelete(uri);
			HttpClient httpClient = WebService.GetClient();
			HttpResponse response = null;

			httpDelete.setHeader("Accept", "application/json");
			httpDelete.setHeader("Content-type", "application/json");
			httpDelete.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
			
			response = httpClient.execute(httpDelete);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e("delete pub feature response", strResponse);
			
			return SUCCESS;
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}	
}
