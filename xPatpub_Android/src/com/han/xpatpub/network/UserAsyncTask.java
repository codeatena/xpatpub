package com.han.xpatpub.network;

import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.LoginActivity;
import com.han.xpatpub.model.Pub;
import com.han.xpatpub.utility.TimeUtility;

public class UserAsyncTask extends AsyncTask<String, Integer, Integer> {

	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public static final String ACTION_LOGIN = "action_login";
	public static final String ACTION_REGISTER = "action_register";
	
	public static final String ACTION_USER_PUB = "action_my_pub";
	public static final String ACTION_UPDATE_USER_PRIVACY = "action_update_user_privacy";
	public static final String ACTION_UPDATE_USER_LOCATION = "action_update_user_location";
	
	public static final String URL_LOGIN = "http://173.248.174.4:8080/rest/xpatpub/tblusers?app_name=TestApp";
	public static final String URL_REGISTER = "http://173.248.174.4:8080/rest/xpatpub/tblusers?app_name=TestApp";
	public static final String URL_USER_PUB = "http://173.248.174.4:8080/rest/xpatpub/tblpubs?app_name=TestApp&include_count=true";
	public static final String URL_UPDATE_USER_PRIVACY = "http://173.248.174.4:8080/rest/xpatpub/tblusers?app_name=TestApp";
	public static final String URL_UPDATE_USER_LOCATION = "http://173.248.174.4:8080/rest/xpatpub/tblusers?app_name=TestApp";
	
    public ProgressDialog dlgLoading;
    public String curAction;
    
	public Context parent;
    public int nResult;
	
	public UserAsyncTask(Context context) {
		parent = context;
	}
	
	@Override
	protected Integer doInBackground(String... params) {
		// TODO Auto-generated method stub
		nResult = FAIL;
		
		curAction = params[0];
		
		if (curAction.equals(ACTION_LOGIN)) {
			nResult = login(params[1]);
		}
		if (curAction.equals(ACTION_REGISTER)) {
			String userName = params[1];
			String userToken = params[2];
			String userEmail = params[3];
			String userType = params[4];
			String userCritic = params[5];
			String userFounder = params[6];
			nResult = register(userName, userToken, userEmail, userType, userCritic, userFounder);
		}
		if (curAction.equals(ACTION_USER_PUB)) {
			nResult = readMyPubList();
		}
		if (curAction.equals(ACTION_UPDATE_USER_PRIVACY)) {
			String strPrivacy = params[1];
			nResult = updateUserPrivacy(strPrivacy);
		}
		if (curAction.equals(ACTION_UPDATE_USER_LOCATION)) {
			nResult = updateUserLocation();
		}
		return nResult;
	}
	
	@Override
	protected void onPreExecute() {
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage("\tLoading...");
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
        dlgLoading.show();
	}

	protected void onPostExecute(Integer result) {
		if (dlgLoading.isShowing())
			dlgLoading.dismiss();
		
		if (curAction.equals(ACTION_LOGIN)) {
			if (nResult == SUCCESS) {
				LoginActivity loginActivity = (LoginActivity) parent;
				loginActivity.successLogin();
			}
			if (nResult == FAIL) {
				LoginActivity loginActivity = (LoginActivity) parent;
				loginActivity.register();
			}
		}
		if (curAction.equals(ACTION_REGISTER)) {
			if (nResult == SUCCESS) {
				LoginActivity loginActivity = (LoginActivity) parent;
				loginActivity.reLogin();
			}
			if (nResult == FAIL) {
				
			}
		}
		if (curAction.equals(ACTION_USER_PUB)) {
			
		}
		if (curAction.equals(ACTION_UPDATE_USER_LOCATION)) {
			
		}
	}
	
	public Integer login(String strUserName) {
		try {
//			String url = URL_LOGIN + "&filter=username='" + strUserName + "'";
			String uri = Uri.parse(URL_LOGIN)
	                .buildUpon()
	                .appendQueryParameter("filter", "username='" + strUserName + "'")
	                .build().toString();
			Log.i("GET login Url = ", uri);
			HttpGet httpGet = new HttpGet(uri);
			HttpClient httpClient = GetClient();
			HttpResponse response = null;
 
//            httpGet.setEntity(se);
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.i(curAction, strResponse);
			
			Integer integer = parseLogin(strResponse);
			
			return integer;
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
	
	public Integer parseLogin(String jsonStr) {
		try {
			JSONObject jsonObj = new JSONObject(jsonStr);
			
			JSONArray arrJsonUser = jsonObj.getJSONArray("record");
			if (arrJsonUser.length() == 0) {
				return FAIL;
			}
			JSONObject jsonUser = arrJsonUser.getJSONObject(0);
			
			GlobalData.currentUser.userName 	= jsonUser.getString("userName");
			GlobalData.currentUser.userID 		= jsonUser.getString("userId");
			GlobalData.currentUser.email 		= jsonUser.getString("userEmail");
			GlobalData.currentUser.userToken	= jsonUser.getString("userToken");
			GlobalData.currentUser.userType 	= jsonUser.getString("userType");
			GlobalData.currentUser.userLat 		= jsonUser.getString("userLat");
			GlobalData.currentUser.userLng 		= jsonUser.getString("userLong");
			GlobalData.currentUser.userCritic 	= jsonUser.getString("userCritic");
			GlobalData.currentUser.userFounder 	= jsonUser.getString("userFounder");

			Log.i("CURRENT USER ID", GlobalData.currentUser.userID);
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}

	public Integer register(String userName, String userToken, String userEmail, String userType, 
			String userCritic, String userFounder) {
		
		try {
			HttpPost httpPost = new HttpPost(URL_REGISTER);
			HttpClient httpClient = GetClient();
			HttpResponse response = null;
			
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("userName", userName);
            jsonObject.accumulate("userToken", userToken);
            jsonObject.accumulate("userEmail", userEmail);
            jsonObject.accumulate("userType", userType);
            jsonObject.accumulate("userLat", Double.toString(GlobalData.curLat));
            jsonObject.accumulate("userLong", Double.toString(GlobalData.curLng));
            jsonObject.accumulate("userCritic", userCritic);
            jsonObject.accumulate("userprivacy", 1);
            jsonObject.accumulate("userFounder", 1);
            jsonObject.accumulate("userlastlogin", TimeUtility.getCurrentTime());
            jsonObject.accumulate("session_id", GlobalData.currentUser.session_id);

            String json = jsonObject.toString();
            StringEntity se = new StringEntity(json);
 
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpPost);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			return SUCCESS;
			
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
	
	public Integer readMyPubList() {
		try {
//			String url = URL_LOGIN + "&filter=username='" + strUserName + "'";
			String uri = Uri.parse(URL_USER_PUB)
	                .buildUpon()
	                .appendQueryParameter("filter", "pubFounder=" + GlobalData.currentUser.userID)
	                .build().toString();
			
			Log.e("GET MY pub Data Url = ", uri);
			HttpGet httpGet = new HttpGet(uri);
			HttpClient httpClient = GetClient();
			HttpResponse response = null;
 
//            httpGet.setEntity(se);
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpGet);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			
			return parseMyPubs(strResponse);
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
		}
	}
	
	public Integer parseMyPubs(String jsonStr) {
		
		try {
			JSONObject jsonObj = new JSONObject(jsonStr);
			
			JSONArray arrJsonPub = jsonObj.getJSONArray("record");
			ArrayList<Pub> arrPub = new ArrayList<Pub>();
			
			for (int i = 0; i < arrJsonPub.length(); i++) {
				
				JSONObject jsonPub = arrJsonPub.getJSONObject(i);
				Pub pub = new Pub();
				
				pub.id = jsonPub.getInt("pubId");
				pub.name = jsonPub.getString("pubName");
				pub.lat = jsonPub.getDouble("pubLat");
				pub.lng = jsonPub.getDouble("pubLong");
				pub.type = jsonPub.getInt("pubCategory");
//				pub.type = jsonPub.getInt("");
				
				pub.isHaveLiveMusic = (jsonPub.getInt("pubHaveLiveMusic") == 1) ? true : false;
				pub.isHaveTVSports = (jsonPub.getInt("pubHaveTVSports") == 1) ? true : false;
				pub.isHavePubGames = (jsonPub.getInt("pubHavePoolDarts") == 1) ? true : false;
//				pub.dis_min = Double.parseDouble(jsonPub.getString("distance_in_mi"));
				
//				pub.avgRating = jsonObj.getString("avgRating");

//				JSONArray arrJsonRate = jsonPub.getJSONArray("pubRating");
//				
//				pub.arrRate = new ArrayList<Rate>();
//				double avgRate = 0;
//				for (int j = 0; j < arrJsonRate.length(); j++) {
//
//					JSONObject jsonRate = arrJsonRate.getJSONObject(j);
//					
//					Rate rate = new Rate();
//					rate.name = jsonRate.getString("rateCategoryName");
//					rate.avgRate = jsonRate.getDouble("avgRating");
//					avgRate += rate.avgRate;
//					pub.arrRate.add(rate);
//				}
				
//				pub.avgRating = (double) ((double) avgRate / (double) pub.arrRate.size());
				arrPub.add(pub);
			}
			
			GlobalData.arrMyPub = arrPub;
			
			Log.e("my pub count", Integer.toString(arrPub.size()));
			return SUCCESS;
		} catch (Exception e) {
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
	
	public Integer updateUserPrivacy(String strPrivacy) {
		try {
			HttpPatch httpPatch = new HttpPatch(URL_UPDATE_USER_PRIVACY);
			HttpClient httpClient = GetClient();
			HttpResponse response = null;
			
			JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("userId", GlobalData.currentUser.userID);
            jsonObject.accumulate("userPrivacy", strPrivacy);
            jsonObject.accumulate("session_id", GlobalData.currentUser.session_id);

            String json = jsonObject.toString();
            StringEntity se = new StringEntity(json);
 
            httpPatch.setEntity(se);
            httpPatch.setHeader("Accept", "application/json");
            httpPatch.setHeader("Content-type", "application/json");
            httpPatch.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpPatch);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			
			return SUCCESS;
			
		} catch (Exception e) {
			Log.e("ClientProtocolException", e.toString());
			return FAIL;
		}
	}
	
	public Integer updateUserLocation() {
		
		try {
			HttpPatch httpPatch = new HttpPatch(URL_UPDATE_USER_LOCATION);
			HttpClient httpClient = GetClient();
			HttpResponse response = null;
			
			JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("userId", GlobalData.currentUser.userID);
            jsonObject.accumulate("userLat", GlobalData.curLat);
            jsonObject.accumulate("userLong", GlobalData.curLng);
            jsonObject.accumulate("userLastLogin", TimeUtility.getCurrentTime());
            
            JSONArray arrJsonRecord = new JSONArray();
            arrJsonRecord.put(jsonObject);
            
            JSONObject jsonRecord = new JSONObject();
            jsonRecord.accumulate("record", arrJsonRecord);
            
            String json = jsonRecord.toString();
            StringEntity se = new StringEntity(json);
 
            httpPatch.setEntity(se);
            httpPatch.setHeader("Accept", "application/json");
            httpPatch.setHeader("Content-type", "application/json");
            httpPatch.setHeader("X-DreamFactory-Session-Token", GlobalData.currentUser.session_id);
            
			response = httpClient.execute(httpPatch);
			
			String strResponse = EntityUtils.toString(response.getEntity());
			Log.e(curAction, strResponse);
			
			return SUCCESS;
			
		} catch (Exception e) {
			Log.e("ClientProtocolException", e.toString());
			return FAIL;
		}
	}
	
	public static DefaultHttpClient GetClient() {
		DefaultHttpClient ret = null;

		// sets up parameters
		HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params, "utf-8");
		params.setBooleanParameter("http.protocol.expect-continue", false);

		// registers schemes for both http and https
		SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		registry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
		
		ThreadSafeClientConnManager manager = new ThreadSafeClientConnManager(params, registry);
		ret = new DefaultHttpClient(manager, params);
		
		return ret;
	}
}
