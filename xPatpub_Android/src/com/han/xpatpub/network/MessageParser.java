package com.han.xpatpub.network;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.model.Coupon;
import com.han.xpatpub.model.Message;
import com.han.xpatpub.model.Pub;

public class MessageParser extends Parser{
	
	public static Integer parseMyMessages(String strResponse) {
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			JSONArray arrJsonMessage = jsonObj.getJSONArray("record");
			ArrayList<Message> arrMessage = new ArrayList<Message>();
			
			for (int i = 0; i < arrJsonMessage.length(); i++) {
				JSONObject jsonMessage = arrJsonMessage.getJSONObject(i);
				
				Message message = new Message();
				message.msgID 			= jsonMessage.getInt("msgId");
				message.msgText 		= jsonMessage.getString("msgText");
				message.msgCreatedDate 	= jsonMessage.getString("msgCreatedDate");
				message.msgStatus 		= jsonMessage.getInt("msgStatus");
				message.msgSenderID		= jsonMessage.getInt("msgSenderId");
				message.msgReceiverID 	= jsonMessage.getInt("msgReceiverId");
				message.msgPubID		= jsonMessage.getInt("msgPubId");
				message.msgCouponID		= jsonMessage.getInt("msgCouponId");
				message.msgCouponUsed 	= jsonMessage.getString("msgCouponUsed");
				
				JSONObject jsonPub = jsonMessage.getJSONObject("tblpubs_by_msgPubId");
				Pub pub = new Pub();

				pub.id = jsonPub.getInt("pubId");
				pub.name = jsonPub.getString("pubName");
				pub.lat = jsonPub.getDouble("pubLat");
				pub.lng = jsonPub.getDouble("pubLong");
				pub.country = jsonPub.getInt("pubCountry");
				pub.category = jsonPub.getInt("pubCategory");
				pub.type = jsonPub.getInt("pubType");
				pub.iconUrl = jsonPub.getString("pubIcon");
				pub.isHaveLiveMusic = (jsonPub.getInt("pubHaveLiveMusic") == 1);
				pub.isHaveTVSports = (jsonPub.getInt("pubHaveTVSports") == 1);
				pub.isHavePubGames = (jsonPub.getInt("pubHavePoolDarts") == 1);
				pub.recommondYes = jsonPub.getInt("pubRecommondYes");
				pub.recommondNo = jsonPub.getInt("pubRecommondNo");
				pub.totalView = jsonPub.getInt("pubTotalViews");
				pub.founder = jsonPub.getInt("pubFounder");
				pub.owner = jsonPub.getInt("pubOwner");
				
				message.ownPub = pub;
				
				JSONObject jsonCoupon = jsonMessage.getJSONObject("tblcoupon_by_msgCouponId");
				Coupon coupon = new Coupon();

				coupon.id = jsonCoupon.getInt("couponId");
				coupon.couponType = jsonCoupon.getString("couponType");
				coupon.couponCode = jsonCoupon.getString("couponCode");
				coupon.couponDescription = jsonCoupon.getString("couponDescription");
				coupon.couponStartDate = jsonCoupon.getString("couponStartDate");
				coupon.couponExpireDate = jsonCoupon.getString("couponExpireDate");
				coupon.couponUsesLimit = jsonCoupon.getInt("couponUsesLimit");
				coupon.couponUserLimit = jsonCoupon.getInt("couponUserLimit");
				coupon.couponStatus = jsonCoupon.getInt("couponStatus");
				coupon.couponUserId = jsonCoupon.getInt("couponUserId");
				coupon.couponPubId = jsonCoupon.getInt("couponPubId");
				coupon.couponName = jsonCoupon.getString("couponName");
				
				message.ownCoupon = coupon;
				
				arrMessage.add(message);
			}
			
			GlobalData.arrMyMessage = arrMessage;
			Log.e("my message count", Integer.toString(arrMessage.size()));
			
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
}
