package com.han.xpatpub;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.graphics.Bitmap;

import com.han.xpatpub.model.Advertise;
import com.han.xpatpub.model.Coupon;
import com.han.xpatpub.model.Message;
import com.han.xpatpub.model.Pub;
import com.han.xpatpub.model.User;

public class GlobalData {
	public static User currentUser = new User();
	
	public static double curLat;
	public static double curLng;
	
	public static double MILES_FOR_METER_10			= 0.0062137;
	public static double MILES_FOR_WALK_ONLY 		= 0.4;
	public static double MILES_FOR_CAB_IT	 		= 3.3;

	public static String SEARCH_TYPE_NONE			= "none";
	public static String SEARCH_TYPE_WALK_ONLY 		= "walk_only";
	public static String SEARCH_TYPE_CAB_IT 		= "cab_it";
	
	public static String PUB_FEATURE_LIVE_MUSIC 	= "pubHaveLiveMusic";
	public static String PUB_FEATURE_TV_SPORTS		= "pubHaveTVSports";
	public static String PUB_FEATURE_PUB_GAMES		= "pubHavePoolDarts";
	
	public static String isoCountryCode;
	public static String countryNumber;
	
	public static ArrayList<Pub> arrAllPub = new ArrayList<Pub>();
	public static ArrayList<Pub> arrMyPub = new ArrayList<Pub>();
	public static ArrayList<Message> arrMyMessage = new ArrayList<Message>();
	public static ArrayList<Coupon> arrMyCoupon = new ArrayList<Coupon>();
	public static ArrayList<Advertise> arrAdvertise = new ArrayList<Advertise>();
	public static ArrayList<User> arrSearchUser = new ArrayList<User>();
	
	public static Pub ownPub = new Pub();
	
	public static String curCityName = "";
	
	public static Bitmap bmpProfile;
	
	public static String getCurrentTime() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String date = df.format(Calendar.getInstance().getTime());
		return date;
	}
	
	public static double getRadius(String searchType) {
		if (searchType.equals(SEARCH_TYPE_WALK_ONLY)) {
			return 0.4;
		}
		if (searchType.equals(SEARCH_TYPE_CAB_IT)) {
			return 3.3;
		}
		
		return 0.4;
	}
	
	public static Pub getPub(int pubID) {
		
		for (int i = 0; i < arrAllPub.size(); i++) {
			Pub pub = arrAllPub.get(i);
			if (pub.id == pubID) {
				return pub;
			}
		}
		
		return null;
	}
}
