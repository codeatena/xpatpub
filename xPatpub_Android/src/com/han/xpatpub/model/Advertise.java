package com.han.xpatpub.model;

public class Advertise {
	
	public int id;
	public String adIcon;
	public double lat;
	public double lng;
	public String time;
	public String title;
	public String type;
	public String link;
}
