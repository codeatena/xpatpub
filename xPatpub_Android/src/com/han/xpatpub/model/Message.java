package com.han.xpatpub.model;

public class Message {
	
	public int msgID;
	public String msgText;
	public String msgCreatedDate;
	public String msgExpDate;
	public int msgStatus;
	public int msgSenderID;
	public int msgReceiverID;
	public int msgPubID;
	public int msgCouponID;
	public String msgCouponUsed;
	public Pub ownPub;
	public Coupon ownCoupon;
}