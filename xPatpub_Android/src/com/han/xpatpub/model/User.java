package com.han.xpatpub.model;

public class User {
	public String userID;
	public String firstName;
	public String lastName;
	public String userName;
	public String email;
	public String userToken;
	public String userType = "1";
	public String userLat;
	public String userLng;
	public String userCritic = "1";
	public String userFounder = "";
	public String session_id;
	public String userPrivacy = "1";
	public String userLastLogin;
	
	public static String getLabelPubType(String strType) {
		
		if (strType.equals("1")) {
			return "Pub Patron";
		}
		if (strType.equals("2")) {
			return "Pub Owner";
		}
		
		return "Pub Patron";
	}
}