package com.han.xpatpub.model;

public class Coupon {
	public int 		id;
	public String 	couponType;
	public String 	couponCode;
	public String 	couponDescription;
	public String 	couponStartDate;
	public String 	couponExpireDate;
	public int		couponUsesLimit;
	public int 		couponUserLimit;
	public int 		couponStatus;
	public int		couponUserId;
	public int 		couponPubId;
	public String 	couponName;
}