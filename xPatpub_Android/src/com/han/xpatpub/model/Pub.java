package com.han.xpatpub.model;

import java.util.ArrayList;

public class Pub {

	public static int NONE = -1;
	public Pub() {
		
	}
	public int id;
	public String name;
	public double lat;
	public double lng;
	public double dis_min;
	public int country;
	public int category;
	public int type;
	
	public boolean isHaveLiveMusic;
	public boolean isHaveTVSports;
	public boolean isHavePubGames;
	
	public String iconUrl;
	public int recommondYes;
	public int recommondNo;
	public int totalView;
	public int founder;
	public int owner;
	
	public int nVibe;
	public int nTunes;
	
	public boolean isGames = false;
	public boolean isDarts = false;
	public boolean isPool = false;
	public boolean isCards = false;
	public boolean isShuffleboard = false;
	
	public int nAles;
	
	public double avgRating;
	
	public ArrayList<Rate> arrRate = new ArrayList<Rate>();
	
	public static String getTypeName(int id) {
		String[] names = {"Western", "Sport", "Irish", "Neighborhood", "Taphouse"};
		
		return names[id - 1];
	}
	
	public static int getTypeIDFromName(String name) {
		String[] names = {"Western", "Sport", "Irish", "Classic", "Taphouse"};
		
		for (int i = 0; i < names.length; i++) {
			if (name.equals(names[i])) {
				return i + 1;
			}
		}
		
		return 1;
	}
	
	public static int getHighestNumber(String strArrNumber) {
		if (strArrNumber.equals("") || strArrNumber == null) return NONE;
		
		String strNumber[] = strArrNumber.split(",");
		int nMax = NONE;
		
		for (int i = 0; i < strNumber.length; i++) {
			int n = Integer.parseInt(strNumber[i]);
			if (n > nMax) nMax = n;
		}
		
		return nMax;
	}
}
