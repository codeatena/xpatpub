package com.han.xpatpub.addpub;

import com.han.xpatpub.R;
import com.han.xpatpub.feature.ActionMenu;
import com.han.xpatpub.feature.AlesMenu;
import com.han.xpatpub.feature.TunesMenu;
import com.han.xpatpub.feature.VibeMenu;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

public class ChoosePubFeature {
	
	public static int MENUITEM_NONE = 0;

	public Activity parent;
	public View view;
	
	RelativeLayout rlytVibeMenuButton;
	RelativeLayout rlytTunesMenuButton;
	RelativeLayout rlytActionMenuButton;
	RelativeLayout rlytAlesMenuButton;
	
	RelativeLayout rlytVibeMenuBar;
	RelativeLayout rlytTunesMenuBar;
	RelativeLayout rlytActionMenuBar;
	RelativeLayout rlytAlesMenuBar;

	VibeMenu viewVibeMenuBar;
	TunesMenu viewTunesMenuBar;
	ActionMenu viewActionMenuBar;
	AlesMenu viewAlesMenuBar;
	
	int nCurMenu;
	
	public static final int MENU_NONE = 0;
	public static final int MENU_VIBE = 1;
	public static final int MENU_TUNES = 2;
	public static final int MENU_ACTION = 3;
	public static final int MENU_ALES = 4;
	
	public ChoosePubFeature(Context context) {
		// TODO Auto-generated constructor stub
		parent = (Activity) context;
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		LayoutInflater inflater = parent.getLayoutInflater();
    	view = inflater.inflate(R.layout.view_choose_pub_feature, null);
    	
		rlytVibeMenuButton = (RelativeLayout) view.findViewById(R.id.vibe_menu_button_relativeLayout);
		rlytTunesMenuButton = (RelativeLayout) view.findViewById(R.id.tunes_menu_button_relativeLayout);
		rlytActionMenuButton = (RelativeLayout) view.findViewById(R.id.action_menu_button_relativeLayout);
		rlytAlesMenuButton = (RelativeLayout) view.findViewById(R.id.ales_menu_button_relativeLayout);
		
		rlytVibeMenuBar = (RelativeLayout) view.findViewById(R.id.vibe_menubar_relativeLayout);
		rlytTunesMenuBar = (RelativeLayout) view.findViewById(R.id.tunes_menubar_relativeLayout);
		rlytActionMenuBar = (RelativeLayout) view.findViewById(R.id.action_menubar_relativeLayout);
		rlytAlesMenuBar = (RelativeLayout) view.findViewById(R.id.ales_menubar_relativeLayout);
		
		viewVibeMenuBar = new VibeMenu(parent, this);
		viewTunesMenuBar = new TunesMenu(parent, this);
		viewActionMenuBar = new ActionMenu(parent, this);
		viewAlesMenuBar = new AlesMenu(parent, this);
		
		rlytVibeMenuBar.addView(viewVibeMenuBar.view);
		rlytTunesMenuBar.addView(viewTunesMenuBar.view);
		rlytActionMenuBar.addView(viewActionMenuBar.view);
		rlytAlesMenuBar.addView(viewAlesMenuBar.view);
	}
	
	private void initValue() {
		initMenu();
	}
	
	private void initEvent() {
		rlytVibeMenuButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selMenu(MENU_VIBE);
			}
        });
		rlytTunesMenuButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selMenu(MENU_TUNES);
			}
        });
		rlytActionMenuButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selMenu(MENU_ACTION);
			}
        });
		rlytAlesMenuButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selMenu(MENU_ALES);
			}
        });
	}
	
	public void initMenu() {
		nCurMenu = MENU_NONE;

		rlytVibeMenuBar.setVisibility(View.GONE);
		rlytTunesMenuBar.setVisibility(View.GONE);
		rlytActionMenuBar.setVisibility(View.GONE);
		rlytAlesMenuBar.setVisibility(View.GONE);
		
		rlytVibeMenuButton.setSelected(false);
		rlytTunesMenuButton.setSelected(false);
		rlytActionMenuButton.setSelected(false);
		rlytAlesMenuButton.setSelected(false);
	}
	
	private void selMenu(int nMenu) {
		if (nMenu == nCurMenu) {
			initMenu();
			return;
		}
		
		initMenu();
		nCurMenu = nMenu;
		
		if (nMenu == MENU_VIBE) {
			rlytVibeMenuBar.setVisibility(View.VISIBLE);
			rlytVibeMenuButton.setSelected(true);
		}
		if (nMenu == MENU_TUNES) {
			rlytTunesMenuBar.setVisibility(View.VISIBLE);
			rlytTunesMenuButton.setSelected(true);
		}
		if (nMenu == MENU_ACTION) {
			rlytActionMenuBar.setVisibility(View.VISIBLE);
			rlytActionMenuButton.setSelected(true);
		}
		if (nMenu == MENU_ALES) {
			rlytAlesMenuBar.setVisibility(View.VISIBLE);
			rlytAlesMenuButton.setSelected(true);
		}
	}
	
	public String getStrFeature() {
		
		String strFeature = "";
		
		if (viewVibeMenuBar.nCurVibe != MENUITEM_NONE) {
			strFeature = strFeature + Integer.toString(viewVibeMenuBar.nCurVibe) + ",";
		}
		if (viewTunesMenuBar.nCurTunes != MENUITEM_NONE) {
			strFeature = strFeature + Integer.toString(viewTunesMenuBar.nCurTunes) + ",";
		}
		if (viewActionMenuBar.isGames) {
			strFeature = strFeature + Integer.toString(ActionMenu.GAMES_ON_TV) + ",";
		}
		if (viewActionMenuBar.isDarts) {
			strFeature = strFeature + Integer.toString(ActionMenu.DARTS) + ",";
		}
		if (viewActionMenuBar.isPool) {
			strFeature = strFeature + Integer.toString(ActionMenu.POOL) + ",";
		}
		if (viewActionMenuBar.isCards) {
			strFeature = strFeature + Integer.toString(ActionMenu.CARDS) + ",";
		}
		if (viewActionMenuBar.isShuffleboard) {
			strFeature = strFeature + Integer.toString(ActionMenu.SHUFFLEBOARD) + ",";
		}
		if (viewAlesMenuBar.nCurAles != MENUITEM_NONE) {
			strFeature = strFeature + Integer.toString(viewAlesMenuBar.nCurAles) + ","; 
		}
		
		return strFeature;
	}
}
