package com.han.xpatpub.setting;

import java.util.ArrayList;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.R;
import com.han.xpatpub.model.User;
import com.loopj.android.image.SmartImageView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class SearchPatronActivity extends Activity {
	
	public static final int REQUEST_CODE_COUPON = 1003;
	public static final int SEND_COUPON_BUTTON_INDEX = 5000;

	ListView lstUser;
	
	SmartImageView imgProfile;
	TextView txtUserName;
	Button btnBack;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_patrons);
	    
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		lstUser = (ListView) findViewById(R.id.user_listView);
		
		imgProfile = (SmartImageView) findViewById(R.id.my_profile_imageView);
		txtUserName = (TextView) findViewById(R.id.myname_textView);
		btnBack = (Button) findViewById(R.id.pub_patron_back_button);
	}
	
	private void initValue() {
		lstUser.setAdapter(new UserAdapter(this, R.layout.row_user, GlobalData.arrSearchUser));
		
		txtUserName.setText(GlobalData.currentUser.firstName + " " + GlobalData.currentUser.lastName);
		imgProfile.setImageBitmap(GlobalData.bmpProfile);
	}
	
	private void initEvent() {
		btnBack.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
        });
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_COUPON) {
			if (resultCode == RESULT_OK) {
				setResult(RESULT_OK);
				finish();
			}
		}
	}
	
	public class UserAdapter extends ArrayAdapter<User> {
    	
    	ArrayList<User> arrData;
    	int row_id;

		public UserAdapter(Context context, int _row_id, ArrayList<User> _arrData) {
			super(context, _row_id, _arrData);
			// TODO Auto-generated constructor stub
			row_id = _row_id;
			arrData = _arrData;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
	        View row = convertView;
	        LayoutInflater inflater = SearchPatronActivity.this.getLayoutInflater();
	        row = inflater.inflate(row_id, parent, false);
	        
	        TextView txtUserName = (TextView) row.findViewById(R.id.row_name_textView);
//	        TextView txtMsgDesc = (TextView) row.findViewById(R.id.advertise_type_textView);
//	        SmartImageView imgAdIcon = (SmartImageView) row.findViewById(R.id.pub_owner_icon_imageView);
	        Button btnSendCoupon = (Button) row.findViewById(R.id.change_pub_owner_icon_button);
	        
	        btnSendCoupon.setId(SEND_COUPON_BUTTON_INDEX + position);

	        User user = arrData.get(position);
//	        Pub pub = GlobalData.getPub(message.msgPubID);
	        txtUserName.setText(user.userName);
	        
	        btnSendCoupon.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View v) {
					int position = v.getId() - SEND_COUPON_BUTTON_INDEX;
			        User user = arrData.get(position);
			        CouponActivity.userSend = user;
			        SearchPatronActivity.this.startActivity(new Intent(SearchPatronActivity.this, CouponActivity.class));
				}
	        });
			
	        return row;
		}
    }
}
