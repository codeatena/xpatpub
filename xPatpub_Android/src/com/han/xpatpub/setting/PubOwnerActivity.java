package com.han.xpatpub.setting;

import java.io.File;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.R;
import com.han.xpatpub.network.FileAsynTask;
import com.han.xpatpub.network.MyAsyncTask;
import com.han.xpatpub.utility.CameraUtility;
import com.loopj.android.image.SmartImageView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PubOwnerActivity extends Activity {
	
	Button btnSearchPatrons;
	Button btnChangePubIcons;
	
	TextView txtPubOwnerName;
	SmartImageView imgPubIcon;

	SmartImageView imgProfile;
	TextView txtUserName;
	Button btnBack;
	
	private Uri fileUri;
	public static final int REQUEST_CODE_SEARCH_PATRON = 1001;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pub_owner);
	    
		initWidget();
		initValue();
		initEvent();
	}
	
	public void initWidget() {
		txtUserName = (TextView) findViewById(R.id.myname_textView);
		imgProfile = (SmartImageView) findViewById(R.id.my_profile_imageView);
		btnBack = (Button) findViewById(R.id.pub_patron_back_button);
		
		txtPubOwnerName = (TextView) findViewById(R.id.row_name_textView);
		imgPubIcon = (SmartImageView) findViewById(R.id.pub_owner_icon_imageView);
		
		btnSearchPatrons = (Button) findViewById(R.id.pub_owner_search_patrons_button);
		btnChangePubIcons = (Button) findViewById(R.id.change_pub_owner_icon_button);
	}
	
	public void initValue() {
		txtUserName.setText(GlobalData.currentUser.firstName + " " + GlobalData.currentUser.lastName);
		imgProfile.setImageBitmap(GlobalData.bmpProfile);
		
		txtPubOwnerName.setText(GlobalData.ownPub.name);
		imgPubIcon.setImageUrl(GlobalData.ownPub.iconUrl);
	}
	
	public void initEvent() {

		btnSearchPatrons.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(PubOwnerActivity.this, SearchPatronActivity.class), REQUEST_CODE_SEARCH_PATRON);
			}
        });
		btnChangePubIcons.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				captureImage();
			}
        });
		btnBack.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
        });
	}
	
	public void captureImage() {
		File f = null;
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 
        fileUri = CameraUtility.getOutputMediaFileUri(CameraUtility.MEDIA_TYPE_IMAGE);
        
        try {
//            f = CameraUtility.getPhotoFile();
//            fileUri = Uri.fromFile(f);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } catch (Exception e) {
        	e.printStackTrace();
        	f = null;
        }
 
        startActivityForResult(intent, CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_SEARCH_PATRON) {
			if (resultCode == RESULT_OK)
				finish();
		}
		if (requestCode == CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                previewCapturedImage();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
	}
	
	private void previewCapturedImage() {
        try {
        	
        	BitmapFactory.Options options = new BitmapFactory.Options();

            options.inSampleSize = 8;
 
            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);
 
            imgPubIcon.setImageBitmap(bitmap);
            Log.e("Image File path", fileUri.getPath());
            
            new FileAsynTask(this).execute(FileAsynTask.ACTION_UPDATE_PUB_ICON, fileUri.getPath(), Integer.toString(GlobalData.ownPub.id));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
