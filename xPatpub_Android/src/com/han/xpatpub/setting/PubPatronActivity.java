package com.han.xpatpub.setting;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.R;
import com.han.xpatpub.R.drawable;
import com.han.xpatpub.R.id;
import com.han.xpatpub.R.layout;
import com.han.xpatpub.model.Message;
import com.han.xpatpub.model.Pub;
import com.han.xpatpub.model.User;
import com.han.xpatpub.network.MyAsyncTask;
import com.han.xpatpub.network.UserAsyncTask;
import com.han.xpatpub.pub.ResultActivity;
import com.han.xpatpub.utility.ImageUtility;
import com.loopj.android.image.SmartImageView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PubPatronActivity extends Activity {
	
	SmartImageView imgProfile;
	TextView txtUserName;
	Button btnBack;

	Button btnPrivacySettings;
	Button btnInbox;
	
	RelativeLayout rlytStealthMenuItem;
	RelativeLayout rlytAnonymousMenuItem;
	RelativeLayout rlytPublicMenuItem;
	
	TextView txtBarsFoundedCount;
	TextView txtInboxCount;
	
	LinearLayout llytPrivacySettingMenuBar;
	
	boolean isShowPrivacySettingMenuBar;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pub_patron);
	    
		initWidget();
		initValue();
		initEvent();
	}
	
	public void initWidget() {
		imgProfile = (SmartImageView) findViewById(R.id.my_profile_imageView);
		txtUserName = (TextView) findViewById(R.id.myname_textView);
		btnPrivacySettings = (Button) findViewById(R.id.privacy_settings_button);
		btnBack = (Button) findViewById(R.id.pub_patron_back_button);
		btnInbox = (Button) findViewById(R.id.pub_owner_search_patrons_button);
		
		llytPrivacySettingMenuBar = (LinearLayout) findViewById(R.id.privacy_setting_menubar_linearLayout);
		
		rlytStealthMenuItem = (RelativeLayout) findViewById(R.id.stealth_menuitem_relativeLayout);
		rlytAnonymousMenuItem = (RelativeLayout) findViewById(R.id.anonymous_menuitem_relativeLayout);
		rlytPublicMenuItem = (RelativeLayout) findViewById(R.id.public_menuitem_relativeLayout);
		
		txtBarsFoundedCount = (TextView) findViewById(R.id.bars_founded_count_textView);
		txtInboxCount = (TextView) findViewById(R.id.inbox_count_textView);
	}
	
	public void initValue() {
		txtUserName.setText(GlobalData.currentUser.firstName + " " + GlobalData.currentUser.lastName);
//		imgProfile.setImageUrl("https://graph.facebook.com/" + GlobalData.currentUser.userToken + "/picture");
		
		llytPrivacySettingMenuBar.setVisibility(View.GONE);
		isShowPrivacySettingMenuBar = false;
		
		imgProfile.setImageBitmap(GlobalData.bmpProfile);
		
		txtBarsFoundedCount.setText(Integer.toString(GlobalData.arrMyPub.size()));
		txtInboxCount.setText(Integer.toString(GlobalData.arrMyMessage.size()));
	}
	
	public void initEvent() {
		btnPrivacySettings.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				isShowPrivacySettingMenuBar = !isShowPrivacySettingMenuBar;
				if (isShowPrivacySettingMenuBar) {
					llytPrivacySettingMenuBar.setVisibility(View.VISIBLE);
				} else {
					llytPrivacySettingMenuBar.setVisibility(View.GONE);
				}
			}
        });
		btnBack.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
        });
		btnInbox.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				PubPatronActivity.this.startActivity(new Intent(PubPatronActivity.this, InboxActivity.class));
			}
        });
		rlytStealthMenuItem.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				isShowPrivacySettingMenuBar = false;
				llytPrivacySettingMenuBar.setVisibility(View.GONE);
				new UserAsyncTask(PubPatronActivity.this).execute(UserAsyncTask.ACTION_UPDATE_USER_PRIVACY, "1");
			}
        });
		rlytAnonymousMenuItem.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				isShowPrivacySettingMenuBar = false;
				llytPrivacySettingMenuBar.setVisibility(View.GONE);
				new UserAsyncTask(PubPatronActivity.this).execute(UserAsyncTask.ACTION_UPDATE_USER_PRIVACY, "2");
			}
        });
		rlytPublicMenuItem.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				isShowPrivacySettingMenuBar = false;
				llytPrivacySettingMenuBar.setVisibility(View.GONE);
				new UserAsyncTask(PubPatronActivity.this).execute(UserAsyncTask.ACTION_UPDATE_USER_PRIVACY, "3");
			}
        });
	}
}
