package com.han.xpatpub.setting;

import java.util.ArrayList;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.R;
import com.han.xpatpub.R.id;
import com.han.xpatpub.R.layout;
import com.han.xpatpub.model.Coupon;
import com.han.xpatpub.model.Message;
import com.han.xpatpub.model.Pub;
import com.han.xpatpub.model.User;
import com.han.xpatpub.network.FileAsynTask;
import com.han.xpatpub.network.MyAsyncTask;
import com.han.xpatpub.setting.InboxActivity.MessageAdapter;
import com.han.xpatpub.setting.SearchPatronActivity.UserAdapter;
import com.loopj.android.image.SmartImageView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class CouponActivity extends Activity {

	public final static int SEND_COUPON_BUTTON_INDEX = 7000;
	public static User userSend = new User();

	SmartImageView imgProfile;
	TextView txtUserName;
	Button btnBack;

	ListView lstCoupon;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_coupon);
	    
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {

		imgProfile = (SmartImageView) findViewById(R.id.my_profile_imageView);
		txtUserName = (TextView) findViewById(R.id.myname_textView);
		btnBack = (Button) findViewById(R.id.pub_patron_back_button);
		
		lstCoupon = (ListView) findViewById(R.id.coupon_listView);
	}
	
	private void initValue() {
		lstCoupon.setAdapter(new CouponAdapter(this, R.layout.row_user, GlobalData.arrMyCoupon));
		
		txtUserName.setText(GlobalData.currentUser.firstName + " " + GlobalData.currentUser.lastName);
		imgProfile.setImageBitmap(GlobalData.bmpProfile);
	}
	
	private void initEvent() {
//		btnCouponDesc1.setOnClickListener(new Button.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				showGeneralAlert("Sent Coupon", "Your offer has been sent!");
//			}
//        });
//		btnCouponDesc2.setOnClickListener(new Button.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				showGeneralAlert("Sent Coupon", "Your offer has been sent!");
//			}
//        });
		btnBack.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
        });
	}
	
	public class CouponAdapter extends ArrayAdapter<Coupon> {
    	
    	ArrayList<Coupon> arrData;
    	int row_id;

		public CouponAdapter(Context context, int _row_id, ArrayList<Coupon> _arrData) {
			super(context, _row_id, _arrData);
			// TODO Auto-generated constructor stub
			row_id = _row_id;
			arrData = _arrData;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
	        View row = convertView;
	        LayoutInflater inflater = CouponActivity.this.getLayoutInflater();
	        row = inflater.inflate(row_id, parent, false);
	        
	        TextView txtCouponName = (TextView) row.findViewById(R.id.row_name_textView);
	        TextView txtCouponDesc = (TextView) row.findViewById(R.id.row_desc_textView);

	        Button btnSendCoupon = (Button) row.findViewById(R.id.change_pub_owner_icon_button);
	        
	        btnSendCoupon.setId(SEND_COUPON_BUTTON_INDEX + position);

	        Coupon coupon = arrData.get(position);
	        txtCouponName.setText(coupon.couponName);
	        txtCouponDesc.setText(coupon.couponDescription);
	        
	        btnSendCoupon.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View v) {
					int position = v.getId() - SEND_COUPON_BUTTON_INDEX;
					Coupon coupon = arrData.get(position);
					
					String msgSenderId = GlobalData.currentUser.userID;
					String msgReceiverId = userSend.userID;
					String msgPubId = Integer.toString(GlobalData.ownPub.id);
					String msgCouponId =Integer.toString(coupon.id);
					
					new MyAsyncTask(CouponActivity.this).execute(MyAsyncTask.ACTION_CREATE_MESSAGE, msgSenderId, msgReceiverId, msgPubId, msgCouponId);
				}
	        });
			
	        return row;
		}
    }
}
