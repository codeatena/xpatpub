package com.han.xpatpub.setting;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.R;
import com.han.xpatpub.model.Message;
import com.han.xpatpub.model.Pub;
import com.loopj.android.image.SmartImageView;
import com.han.xpatpub.network.MyAsyncTask;
import com.han.xpatpub.utility.DialogUtility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MessageActivity extends Activity {
	
	SmartImageView imgProfile;
	TextView txtUserName;
	Button btnBack;
	
	TextView txtMsgText;
	Button btnUse;
	SmartImageView imgMsgIcon;
	EditText edtCouponCode;
	
	public static Message curMessage = new Message();
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message);
	    
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		imgProfile = (SmartImageView) findViewById(R.id.my_profile_imageView);
		txtUserName = (TextView) findViewById(R.id.myname_textView);
		btnBack = (Button) findViewById(R.id.pub_patron_back_button);
		
		txtMsgText = (TextView) findViewById(R.id.row_name_textView);
		btnUse = (Button) findViewById(R.id.change_pub_owner_icon_button);
		imgMsgIcon = (SmartImageView) findViewById(R.id.pub_owner_icon_imageView);
		
		edtCouponCode = (EditText) findViewById(R.id.coupon_code_editText);
	}
	
	private void initValue() {
		txtUserName.setText(GlobalData.currentUser.firstName + " " + GlobalData.currentUser.lastName);
		imgProfile.setImageBitmap(GlobalData.bmpProfile);
		
		txtMsgText.setText(curMessage.msgText);
		imgMsgIcon.setImageUrl(curMessage.ownPub.iconUrl);
	}
	
	private void initEvent() {
        btnUse.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isCheckCouponCode()) {
					new MyAsyncTask(MessageActivity.this).execute(MyAsyncTask.ACTION_MARK_MESSAGE, Integer.toString(curMessage.msgID));
				}
			}
        });
        btnBack.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
        });
	}
	
	private boolean isCheckCouponCode() {
		String strCode = edtCouponCode.getText().toString();
		
		if (strCode.isEmpty()) {
			DialogUtility.showGeneralAlert(this, "Error", "Please input code");
			return false;
		}
		if (!strCode.equals(curMessage.ownCoupon.couponCode)) {
			DialogUtility.showGeneralAlert(this, "Error", "This code is invalid");
			return false;
		}
		
		return true;
	}
	
	public void successMarkMessage() {
		new AlertDialog.Builder(this)
	      
		   .setTitle("Success")
		   .setMessage("Coupon has been used. Thanks")
		   .setPositiveButton("OK", new DialogInterface.OnClickListener() {
			   public void onClick(DialogInterface dialog, int which) {
		       // continue with delete
				   finish();
		       }
		    })
		    .show();
	}
}
