package com.han.xpatpub;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.han.xpatpub.network.FileAsynTask;
import com.han.xpatpub.network.MyAsyncTask;
import com.han.xpatpub.network.UserAsyncTask;
import com.han.xpatpub.pub.ResultActivity;
import com.han.xpatpub.utility.LocationUtility;

public class LoginActivity extends Activity implements LocationListener,
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener{
	
	private LocationRequest mLocationRequest;

    // Stores the current instantiation of the location client in this object
    private LocationClient mLocationClient;
    
	private LocationManager locationManager;

	private final String TAG = "facebook";
	
	Button btnLogin;
	
	public static Bitmap profPic;
//	private UiLifecycleHelper uiHelper;	
//	private LoginButton loginButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		// Add code to print out the key hash
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.han.xpatpub", PackageManager.GET_SIGNATURES);
			
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("KeyHash:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}
		
//		uiHelper = new UiLifecycleHelper(this, callback);
//	    uiHelper.onCreate(savedInstanceState);
	    
		initWidget();
		initValue();
		initEvent();
	}
	
	private void fb_login() {
		List<String> permissions = new ArrayList<String>();
		permissions.add("email");
		 Session.openActiveSession(this, true, permissions, new Session.StatusCallback() {

		      // callback when session changes state
		      @Override
		      public void call(Session session, SessionState state, Exception exception) {
		        if (session.isOpened()) {

		          // make request to the /me API
		          Request.newMeRequest(session, new Request.GraphUserCallback() {

		            // callback after Graph API response with user object
		            @Override
		            public void onCompleted(GraphUser user, Response response) {
		              if (user != null) {
		            	  
		            	  GlobalData.currentUser.firstName = user.getFirstName();
		            	  GlobalData.currentUser.lastName = user.getLastName();
		            	  GlobalData.currentUser.userName = user.getName();
		            	  GlobalData.currentUser.email = user.getProperty("email").toString();
		            	  GlobalData.currentUser.userToken = user.getId();
		            	  
		            	  try {
//			            	  URL image_value = new URL("https://graph.facebook.com/" + user.getId() + "/picture");
//			            	  profPic = BitmapFactory.decodeStream(image_value.openConnection().getInputStream());
			            	  
			            	  Log.e(TAG, "username: " + user.getName());
			            	  Log.e(TAG, "email: " + user.getProperty("email").toString());
			            	  Log.e(TAG, "ID: " + user.getId());
		            	  } catch (Exception e) {
		            		  
		            	  }
		            	  
		            	  new UserAsyncTask(LoginActivity.this).execute(UserAsyncTask.ACTION_LOGIN, user.getName());
		              }
		            }
		          }).executeAsync();
		        }
		      }
		    });
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}
	  
	public void initWidget() {
		btnLogin = (Button) findViewById(R.id.coupon_button2);
	}
	
	public void initValue() {
		mLocationRequest = LocationRequest.create();

        /*
         * Set the update interval
         */
        mLocationRequest.setInterval(LocationUtility.UPDATE_INTERVAL_IN_MILLISECONDS);

        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Set the interval ceiling to one minute
        mLocationRequest.setFastestInterval(LocationUtility.FAST_INTERVAL_CEILING_IN_MILLISECONDS);

        mLocationClient = new LocationClient(this, this, this);
	}
	
	public void initEvent() {
		btnLogin.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
//				LoginActivity.this.startActivity(new Intent(LoginActivity.this, ResultActivity.class));
//				finish();
				
				fb_login();
			}
        });
		
//		new MyAsyncTask(LoginActivity.this).execute(MyAsyncTask.ACTION_SESSION, "expatpub@gmail.com", "SmallCats15");
		new MyAsyncTask(LoginActivity.this).execute(MyAsyncTask.ACTION_SESSION, "yogesh.m@lucidsolutions.in", "123456");
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
	   	 
        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        Location location = locationManager.getLastKnownLocation(provider);
//        searchAction(location);
        
        if(location != null) {
            onLocationChanged(location);
        }
        
        locationManager.requestLocationUpdates(provider, 200, 1, this);
	}
	
	public void searchAction(Location location) {
		
		if (location == null) return;
		
		String strLat = Double.toString(location.getLatitude());
		String strLng = Double.toString(location.getLongitude());
		
	    GlobalData.curLat = location.getLatitude();
	    GlobalData.curLng = location.getLongitude();
	    	    
		Log.e("Latitude, Longitude = ", strLat + " " + strLng);
	}
	
	public void successLogin() {
		
	  	 LoginActivity.this.startActivity(new Intent(LoginActivity.this, ResultActivity.class));
	  	 LoginActivity.this.finish();
	}
	
	public void register() {
		new UserAsyncTask(this).execute(UserAsyncTask.ACTION_REGISTER, GlobalData.currentUser.userName, 
				GlobalData.currentUser.userToken, GlobalData.currentUser.email, "1", 
				"1", GlobalData.currentUser.userFounder);
	}
	
	public void reLogin() {
		new UserAsyncTask(this).execute(UserAsyncTask.ACTION_LOGIN, GlobalData.currentUser.userName);
	}

	public void successSession() {
		new MyAsyncTask(LoginActivity.this).execute(MyAsyncTask.ACTION_GET_COUNTRY_CODE_FROM_GEO_DATA, 
			Double.toString(GlobalData.curLat), Double.toString(GlobalData.curLng));
		new FileAsynTask(this).execute(FileAsynTask.ACTION_ADVERTISE_LIST);
	    new FileAsynTask(this).execute(FileAsynTask.ACTION_GET_CURRENT_CITY);
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
		if (location == null) return;
		
	    GlobalData.curLat = location.getLatitude();
	    GlobalData.curLng = location.getLongitude();
	    searchAction(location);
	    
//	    locationManager.removeUpdates(this);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
        Toast.makeText(this, "GPS Connected", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
        Toast.makeText(this, "GPS Disconnected.", Toast.LENGTH_SHORT).show();
	}
	
	public void onStart() {
        super.onStart();
        mLocationClient.connect();
	}
	
	public void onStop() {
        mLocationClient.disconnect();
	    locationManager.removeUpdates(this);
	    super.onStop();
	}
}