package com.han.xpatpub.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class DialogUtility {

	public static void showGeneralAlert(Context context, String title, String message) {
		new AlertDialog.Builder(context)
	      
		   .setTitle(title)
		   .setMessage(message)
		   .setPositiveButton("OK", new DialogInterface.OnClickListener() {
			   public void onClick(DialogInterface dialog, int which) { 
		       // continue with delete
				   
		       }
		    })
		    .show();
	}
}
