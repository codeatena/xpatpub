package com.han.xpatpub.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.util.Log;

public class TimeUtility {
	public static String getCurrentTime() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = df.format(Calendar.getInstance().getTime());
		
		Log.e("userLastLogin", date);
		return date;
	}
	
	public static String getDateAfter(int k) {
		
		Calendar nowCalendar = Calendar.getInstance(); 		
//		Log.e("dayOfWeek", Integer.toString(dayOfWeek));
		
		long nowTime = nowCalendar.getTimeInMillis();
		long afterTime = nowTime + 86400000 * k;
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date afterDate = new Date(afterTime);
		String strDate = df.format(afterDate);
		
		return strDate;	
	}
}
