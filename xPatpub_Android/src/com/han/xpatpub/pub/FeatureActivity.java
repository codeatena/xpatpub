package com.han.xpatpub.pub;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.R;
import com.han.xpatpub.feature.ActionMenu;
import com.han.xpatpub.feature.AlesMenu;
import com.han.xpatpub.feature.TunesMenu;
import com.han.xpatpub.feature.VibeMenu;
import com.han.xpatpub.network.MyAsyncTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FeatureActivity extends Activity {
	
	public static final int MENU_NONE = 0;
	public static final int MENU_VIBE = 1;
	public static final int MENU_TUNES = 2;
	public static final int MENU_ACTION = 3;
	public static final int MENU_ALES = 4;
	
	public static int MENUITEM_NONE = 0;
		
	public int curMenu;

	RelativeLayout rlytVibeMenu;
	RelativeLayout rlytTunesMenu;
	RelativeLayout rlytActionMenu;
	RelativeLayout rlytAlesMenu;
	
	ImageView imvVibeMenu;
	ImageView imvTunesMenu;
	ImageView imvActionMenu;
	ImageView imvAlesMenu;
	
	TextView txtVibeMenu;
	TextView txtTunesMenu;
	TextView txtActionMenu;
	TextView txtAlesMenu;
	
	ImageView imvVibeMenuSelMark;
	ImageView imvTunesMenuSelMark;
	ImageView imvActionMenuSelMark;
	ImageView imvAlesMenuSelMark;
	
	VibeMenu viewVibeMenu;
	TunesMenu viewTunesMenu;
	ActionMenu viewActionMenu;
	AlesMenu viewAlesMenu;
	
	RelativeLayout rlytVibeView;
	RelativeLayout rlytTunesView;
	RelativeLayout rlytActionView;
	RelativeLayout rlytAlesView;
	
	Button btnSearch;
	Button btnBack;
	
	public static String strFeature = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feature);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		rlytVibeMenu = (RelativeLayout) findViewById(R.id.vibe_menu_relativeLayout);
		rlytTunesMenu = (RelativeLayout) findViewById(R.id.tunes_menu_relativeLayout);
		rlytActionMenu = (RelativeLayout) findViewById(R.id.action_menu_relativeLayout);
		rlytAlesMenu = (RelativeLayout) findViewById(R.id.ales_menu_relativeLayout);

		imvVibeMenu = (ImageView) findViewById(R.id.vibe_menu_imageView);
		imvTunesMenu = (ImageView) findViewById(R.id.tunes_menu_imageView);
		imvActionMenu = (ImageView) findViewById(R.id.action_menu_imageView);
		imvAlesMenu = (ImageView) findViewById(R.id.ales_menu_imageView);
		
		txtVibeMenu = (TextView) findViewById(R.id.vibe_menu_textView);
		txtTunesMenu = (TextView) findViewById(R.id.tunes_menu_textView);
		txtActionMenu = (TextView) findViewById(R.id.action_menu_textView);
		txtAlesMenu = (TextView) findViewById(R.id.ales_menu_textView);
		
		imvVibeMenuSelMark = (ImageView) findViewById(R.id.vibe_menu_sel_mark_imageView);
		imvTunesMenuSelMark = (ImageView) findViewById(R.id.tunes_menu_sel_mark_imageView);
		imvActionMenuSelMark = (ImageView) findViewById(R.id.action_menu_sel_mark_imageView);
		imvAlesMenuSelMark = (ImageView) findViewById(R.id.ales_menu_sel_mark_imageView);
		
		rlytVibeView = (RelativeLayout) findViewById(R.id.vibe_menubar_relativeLayout);
		rlytTunesView = (RelativeLayout) findViewById(R.id.tunes_menubar_relativeLayout);
		rlytActionView = (RelativeLayout) findViewById(R.id.action_menubar_relativeLayout);
		rlytAlesView = (RelativeLayout) findViewById(R.id.ales_view_relativeLayout);
		
		viewVibeMenu = new VibeMenu(this);
		viewTunesMenu = new TunesMenu(this);
		viewActionMenu = new ActionMenu(this);
		viewAlesMenu = new AlesMenu(this);
		
		rlytVibeView.addView(viewVibeMenu.view);
		rlytTunesView.addView(viewTunesMenu.view);
		rlytActionView.addView(viewActionMenu.view);
		rlytAlesView.addView(viewAlesMenu.view);
		
		btnSearch = (Button) findViewById(R.id.submit_button);
		btnBack = (Button) findViewById(R.id.pub_patron_back_button);
	}
	
	private void initValue() {
//		curMenu = MENU_NONE;
		
//		String str = Double.toString(GlobalData.curLat) + ", " + Double.toString(GlobalData.curLng);
//		DialogUtility.showGeneralAlert(this, "Current Location", str);
		initMenu();
	}
	
	private void initEvent() {
		rlytVibeMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selMenu(MENU_VIBE);
			}
        });
		rlytTunesMenu.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				selMenu(MENU_TUNES);
			}
        });
		rlytActionMenu.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				selMenu(MENU_ACTION);
			}
        });
		rlytAlesMenu.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				selMenu(MENU_ALES);
			}
        });
		btnSearch.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				strFeature = "";
				
				if (viewVibeMenu.nCurVibe != MENUITEM_NONE) {
					strFeature = strFeature + Integer.toString(viewVibeMenu.nCurVibe) + ",";
				}
				if (viewTunesMenu.nCurTunes != MENUITEM_NONE) {
					strFeature = strFeature + Integer.toString(viewTunesMenu.nCurTunes) + ",";
				}
				if (viewActionMenu.isGames) {
					strFeature = strFeature + Integer.toString(ActionMenu.GAMES_ON_TV) + ",";
				}
				if (viewActionMenu.isDarts) {
					strFeature = strFeature + Integer.toString(ActionMenu.DARTS) + ",";
				}
				if (viewActionMenu.isPool) {
					strFeature = strFeature + Integer.toString(ActionMenu.POOL) + ",";
				}
				if (viewActionMenu.isCards) {
					strFeature = strFeature + Integer.toString(ActionMenu.CARDS) + ",";
				}
				if (viewActionMenu.isShuffleboard) {
					strFeature = strFeature + Integer.toString(ActionMenu.SHUFFLEBOARD) + ",";
				}
				if (viewAlesMenu.nCurAles != MENUITEM_NONE) {
					strFeature = strFeature + Integer.toString(viewAlesMenu.nCurAles) + ","; 
				}
				
				Log.e("search by strFeature origin", strFeature);

				if (!strFeature.equals("")) {
					strFeature = strFeature.substring(0, strFeature.length() - 1);
				}
				
				String strLat = Double.toString(GlobalData.curLat);
				String strLng = Double.toString(GlobalData.curLng);
				
//				String strLat = "44.8300933";
//				String strLng = "-93.4403626";
				
				new MyAsyncTask(FeatureActivity.this).execute(MyAsyncTask.ACTION_SEARCH_PUB, strLat, strLng, "", 
						strFeature, Double.toString(GlobalData.MILES_FOR_CAB_IT));
//				Log.e("search by strFeature cutting", strFeature);
			}
        });
		btnBack.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
//				btnSearch.setFocusableInTouchMode(true);
				finish();
			}
        });
	}
	
	public void initMenu() {
		curMenu = MENU_NONE;

		rlytVibeMenu.setBackgroundResource(R.drawable.general_button_default_back);
		imvVibeMenu.setImageResource(R.drawable.vibe_nor);
		imvVibeMenuSelMark.setVisibility(View.GONE);
		txtVibeMenu.setTextColor(0xffffffff);

		rlytTunesMenu.setBackgroundResource(R.drawable.general_button_default_back);
		imvTunesMenu.setImageResource(R.drawable.tunes_nor);
		imvTunesMenuSelMark.setVisibility(View.GONE);
		txtTunesMenu.setTextColor(0xffffffff);

		rlytActionMenu.setBackgroundResource(R.drawable.general_button_default_back);
		imvActionMenu.setImageResource(R.drawable.action_nor);
		imvActionMenuSelMark.setVisibility(View.GONE);
		txtActionMenu.setTextColor(0xffffffff);

		rlytAlesMenu.setBackgroundResource(R.drawable.general_button_default_back);
		imvAlesMenu.setImageResource(R.drawable.ales_nor);
		imvAlesMenuSelMark.setVisibility(View.GONE);
		txtAlesMenu.setTextColor(0xffffffff);
		
		rlytVibeView.setVisibility(View.GONE);
		rlytTunesView.setVisibility(View.GONE);
		rlytActionView.setVisibility(View.GONE);
		rlytAlesView.setVisibility(View.GONE);
		
	}
	
	private void selMenu(int nMenu) {

		if (nMenu == curMenu) {
			initMenu();
			return;
		}
		
		initMenu();
		curMenu = nMenu;
		
		if (nMenu == MENU_VIBE) {
			rlytVibeMenu.setBackgroundResource(R.drawable.feature_menu_sel_backgroud);
			imvVibeMenu.setImageResource(R.drawable.vibe_sel);
			imvVibeMenuSelMark.setVisibility(View.VISIBLE);
			txtVibeMenu.setTextColor(0xfffcc300);
			rlytVibeView.setVisibility(View.VISIBLE);
		}
		if (nMenu == MENU_TUNES) {
			rlytTunesMenu.setBackgroundResource(R.drawable.feature_menu_sel_backgroud);
			imvTunesMenu.setImageResource(R.drawable.tunes_sel);
			imvTunesMenuSelMark.setVisibility(View.VISIBLE);
			txtTunesMenu.setTextColor(0xfffcc300);
			rlytTunesView.setVisibility(View.VISIBLE);
		}
		if (nMenu == MENU_ACTION) {
			rlytActionMenu.setBackgroundResource(R.drawable.feature_menu_sel_backgroud);
			imvActionMenu.setImageResource(R.drawable.action_sel);
			imvActionMenuSelMark.setVisibility(View.VISIBLE);
			txtActionMenu.setTextColor(0xfffcc300);
			rlytActionView.setVisibility(View.VISIBLE);
		}
		if (nMenu == MENU_ALES) {
			rlytAlesMenu.setBackgroundResource(R.drawable.feature_menu_sel_backgroud);
			imvAlesMenu.setImageResource(R.drawable.ales_sel);
			imvAlesMenuSelMark.setVisibility(View.VISIBLE);
			txtAlesMenu.setTextColor(0xfffcc300);
			rlytAlesView.setVisibility(View.VISIBLE);
		}
	}
	
	public void sucessSearchPub() {
		FeatureActivity.this.startActivity(new Intent(FeatureActivity.this, SearchActivity.class));
	}
}
