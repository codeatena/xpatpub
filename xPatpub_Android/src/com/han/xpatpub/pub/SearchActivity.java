package com.han.xpatpub.pub;

import java.util.ArrayList;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.han.xpatpub.GlobalData;
import com.han.xpatpub.R;
import com.han.xpatpub.model.Pub;
import com.han.xpatpub.utility.LocationUtility;
import com.loopj.android.image.SmartImageView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
//import android.location.Criteria;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SearchActivity extends Activity implements LocationListener,
	GooglePlayServicesClient.ConnectionCallbacks,
    GooglePlayServicesClient.OnConnectionFailedListener {
	
	private static final int PUB_INFO_REQUEST_CODE = 1001;
	private static final int ADD_PUB_REQUEST_CODE  = 1002;
	
	public static String strPubFeature = "";
	public static String strPubType = "";
	
	private GoogleMap mMap;
	private LocationManager locationManager;
	
	private LocationRequest mLocationRequest;
    private LocationClient mLocationClient;

	public static ArrayList<Pub> arrSearchPub = new ArrayList<Pub>();
	
	public static String strSearchType = GlobalData.SEARCH_TYPE_CAB_IT;
	public static int SEE_BUTTON_INDEX = 2000;
	ListView lstPubs;
	
	Button btnHome;
	Button btnAddPub;
	
//	LocationClient mLocationClient;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		
//		mLocationClient = new LocationClient(this, this, this);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	public void initWidget() {
		
//		llytPub = (LinearLayout) findViewById(R.id.pub_info_layout);
		btnHome = (Button) findViewById(R.id.search_home_button);
		btnAddPub = (Button) findViewById(R.id.search_add_pub_button);
		
		lstPubs = (ListView) findViewById(R.id.pubs_listView);
        setUpMapIfNeeded();
	}
	
	public void initValue() {
		mLocationRequest = LocationRequest.create();

        /*
         * Set the update interval
         */
        mLocationRequest.setInterval(LocationUtility.UPDATE_INTERVAL_IN_MILLISECONDS);

        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Set the interval ceiling to one minute
        mLocationRequest.setFastestInterval(LocationUtility.FAST_INTERVAL_CEILING_IN_MILLISECONDS);

        mLocationClient = new LocationClient(this, this, this);
	}
	
	public void searchAction(Location location) {
		
		String strLat = Double.toString(location.getLatitude());
		String strLng = Double.toString(location.getLongitude());
		
	    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
	    
	    float zoom = 15;
	    if (strSearchType == GlobalData.SEARCH_TYPE_CAB_IT) {
	    	zoom = 14;
	    }
	    
	    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
	    
	    GlobalData.curLat = location.getLatitude();
	    GlobalData.curLng = location.getLongitude();

	    mMap.animateCamera(cameraUpdate);
		Log.e("Latitude, Longitude = ", strLat + " " + strLng);
	}
	
	public void initEvent() {		
//		llytPub.setOnClickListener(new Button.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				SearchActivity.this.startActivityForResult(new Intent(SearchActivity.this, PubInfoActivity.class), SEARCH_REQUEST_CODE);
//			}
//        });
		btnHome.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		btnAddPub.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				SearchActivity.this.startActivity(new Intent(SearchActivity.this, AddPubActivity.class));
			}
		});
	}
	
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the MapFragment.
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
            	
                setUpMap();
            }
        }
    }
    
    private void setUpMap() {
    	mMap.setMyLocationEnabled(true);
    	
    	locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    	 
        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        Location location = locationManager.getLastKnownLocation(provider);
//        searchAction(location);
        
        if(location != null){
            onLocationChanged(location);
        }
        locationManager.requestLocationUpdates(provider, 200, 1, this);
    }
    
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    ConnectionResult connectionResult;
    
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
        	case CONNECTION_FAILURE_RESOLUTION_REQUEST :
        /*
         * If the result code is Activity.RESULT_OK, try
         * to connect again
         */
	            switch (resultCode) {
	                case Activity.RESULT_OK :
	                /*
	                 * Try the request again
	                 */
	                break;
	            }
	            break;
        	case PUB_INFO_REQUEST_CODE:
	        	if(resultCode == RESULT_OK) {
	        		
				}
				if (resultCode == RESULT_FIRST_USER) {
					finish();
				}
				break;
        	case ADD_PUB_REQUEST_CODE:
        		
        		Criteria criteria = new Criteria();

                // Getting the name of the best provider
                String provider = locationManager.getBestProvider(criteria, true);

                // Getting Current Location
                Location location = locationManager.getLastKnownLocation(provider);
                searchAction(location);
        		break;
        }
	}
	
	@Override
	public void onLocationChanged(Location location) {
	    searchAction(location);
//	    locationManager.removeUpdates(this);
	}

	@Override
	public void onProviderDisabled(String provider) {
	    // TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
	    // TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	    // TODO Auto-generated method stub
		
	}
	
    @Override
    public void onConnected(Bundle dataBundle) {
        // Display the connection status
        Toast.makeText(this, "GPS Connected", Toast.LENGTH_SHORT).show();
    }
    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onDisconnected() {
        // Display the connection status
        Toast.makeText(this, "GPS Disconnected.", Toast.LENGTH_SHORT).show();
    }
    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            showDialog(connectionResult.getErrorCode());
        }
    }
    
    public void drawPubList() {
    	mMap.clear();
    	arrSearchPub = new ArrayList<Pub>();
		
		for (int i = 0; i < GlobalData.arrAllPub.size(); i++) {
			Pub pub = GlobalData.arrAllPub.get(i);
			
			if (strPubFeature.equals(GlobalData.PUB_FEATURE_LIVE_MUSIC)) {
				if (!pub.isHaveLiveMusic) continue;
			}
			if (strPubFeature.equals(GlobalData.PUB_FEATURE_TV_SPORTS)) {
				if (!pub.isHaveTVSports) continue;
			}
			if (!strPubType.equals("")) {
				if (!strPubType.equals(Integer.toString(pub.type))) {
					continue;
				}
			}
			
			double radius = GlobalData.getRadius(strSearchType);
			if (pub.dis_min > radius) continue;
			
			arrSearchPub.add(pub);
		}
		
    	lstPubs.setAdapter(new PubsAdapter(this, R.layout.row_advertise, arrSearchPub));
    	    	
    	for (int i = 0; i < arrSearchPub.size(); i++) {
    		Pub pub = arrSearchPub.get(i);
    		
    		double lat = pub.lat;
    		double lng = pub.lng;
    		
    		Log.e("Pub postion", Double.toString(lat) + ", " + Double.toString(lng));
    		LatLng pos = new LatLng(lat, lng);

    		MarkerOptions marker = new MarkerOptions().position(pos).title(pub.name);
    		 
    		marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_1 + i));
    		mMap.addMarker(marker);
    	}
    	
//    	lstPubs.setOnItemClickListener(new OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                
//        		Pub pub = arrSearchPub.get(position);
//        	    PubInfoActivity.curPubID = pub.id;
//        	    
//        	    Log.e("Current Pub id", Integer.toString(pub.id));
//            	SearchActivity.this.startActivityForResult(new Intent(SearchActivity.this, PubInfoActivity.class), PUB_INFO_REQUEST_CODE);
//            }
//        });
    }
    
    public void onResume() {
    	super.onResume();
    	drawPubList();
    }
    
    public class PubsAdapter extends ArrayAdapter<Pub> {
    	
    	ArrayList<Pub> arrData;
    	int row_id;

		public PubsAdapter(Context context, int _row_id, ArrayList<Pub> _arrData) {
			super(context, _row_id, _arrData);
			// TODO Auto-generated constructor stub
			row_id = _row_id;
			arrData = _arrData;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
	        View row = convertView;
	        LayoutInflater inflater = SearchActivity.this.getLayoutInflater();
	        row = inflater.inflate(row_id, parent, false);
	        
	        TextView txtPubTitle = (TextView) row.findViewById(R.id.row_name_textView);
	        TextView txtPubType = (TextView) row.findViewById(R.id.row_desc_textView);
	        SmartImageView imgPubIcon = (SmartImageView) row.findViewById(R.id.pub_owner_icon_imageView);
	        Button btnSee = (Button) row.findViewById(R.id.change_pub_owner_icon_button);
	        
	        btnSee.setId(SEE_BUTTON_INDEX + position);

	        Pub pub = arrData.get(position);
	        
	        txtPubTitle.setText(Integer.toString(position + 1) + ". " + pub.name);
	        txtPubType.setText(Pub.getTypeName(pub.type));
	        imgPubIcon.setImageUrl(pub.iconUrl);
	        
	        btnSee.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View v) {
					int pos = v.getId() - SEE_BUTTON_INDEX;

					Pub pub = arrSearchPub.get(pos);
	        	    PubInfoActivity.curPubID = pub.id;
	        	    
	        	    Log.e("Current Pub id", Integer.toString(pub.id));
	            	SearchActivity.this.startActivityForResult(new Intent(SearchActivity.this, PubInfoActivity.class), PUB_INFO_REQUEST_CODE);
				}
	        });
	        
	        return row;
		}
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
        mLocationClient.connect();
    }
    
    @Override
	public void onStop() {
        mLocationClient.disconnect();
	    locationManager.removeUpdates(this);
	    super.onStop();
	}
}