package com.han.xpatpub.pub;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.R;
import com.han.xpatpub.model.Advertise;
import com.han.xpatpub.network.FileAsynTask;
import com.han.xpatpub.network.MyAsyncTask;
import com.han.xpatpub.network.UserAsyncTask;
import com.han.xpatpub.setting.PubOwnerActivity;
import com.han.xpatpub.setting.PubPatronActivity;
import com.han.xpatpub.utility.ImageUtility;
import com.loopj.android.image.SmartImageView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ResultActivity extends Activity {
	
	private static final int DELAY_TIME_HIDE_PUB_TYPE_MENUBAR = 200;
	public static int SEE_BUTTON_INDEX = 1000;

	Button btnMenu;
	TextView txtUserName;
	
	public static SmartImageView imgProfile;
	public static TextView txtUnreadMessageCount;
	
	TextView txtCityName;
	
	LinearLayout llytSrchAllPubsButton;
	RelativeLayout rlytSrchPubTypeMenu;
	
	LinearLayout llytSrchPubTypeMenuBar;
	
	RelativeLayout rlytWesternMenuItem;
	RelativeLayout rlytSportMenuItem;
	RelativeLayout rlytIrishMenuItem;
	RelativeLayout rlytClassicMenuItem;
	RelativeLayout rlytTaphouseMenuItem;

	ListView lstAdvertise;
	
	boolean isOpenPubTypeMenu;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);
		
		initWidget();
		initValue();
		initEvent();
		
		new UserAsyncTask(this).execute(UserAsyncTask.ACTION_UPDATE_USER_LOCATION);
		new UserAsyncTask(this).execute(UserAsyncTask.ACTION_USER_PUB);
		new MyAsyncTask(this).execute(MyAsyncTask.ACTION_USER_MESSAGE);
		
		if (GlobalData.currentUser.userType.equals("2")) {
			new MyAsyncTask(this).execute(MyAsyncTask.ACTION_PUB_OWNER_DETAIL);
			
			String strLat = Double.toString(GlobalData.curLat);
			String strLng = Double.toString(GlobalData.curLng);
			String strRadius = "500";
			
			new FileAsynTask(this).execute(FileAsynTask.ACTION_COUPON_LIST);
			new MyAsyncTask(this).execute(MyAsyncTask.ACTION_USER_LIST, strLat, strLng, strRadius);
		}
	}
	
	public void initWidget() {
		
		btnMenu = (Button) findViewById(R.id.menu_button);
		txtUserName = (TextView) findViewById(R.id.myname_textView);
		
		imgProfile = (SmartImageView) findViewById(R.id.my_profile_imageView);
		txtUnreadMessageCount = (TextView) findViewById(R.id.bars_founded_count_textView);
		
		txtCityName = (TextView) findViewById(R.id.current_city_textView);
		
		llytSrchAllPubsButton = (LinearLayout) findViewById(R.id.choose_pub_type_linearLayout);
		rlytSrchPubTypeMenu = (RelativeLayout) findViewById(R.id.srch_pub_type_menu_relativeLayout);
		lstAdvertise = (ListView) findViewById(R.id.advertise_listView);
		
		llytSrchPubTypeMenuBar = (LinearLayout) findViewById(R.id.privacy_setting_menubar_linearLayout);

		rlytWesternMenuItem = (RelativeLayout) findViewById(R.id.stealth_menuitem_relativeLayout);
		rlytSportMenuItem = (RelativeLayout) findViewById(R.id.anonymous_menuitem_relativeLayout);
		rlytIrishMenuItem = (RelativeLayout) findViewById(R.id.public_menuitem_relativeLayout);
		rlytClassicMenuItem = (RelativeLayout) findViewById(R.id.classic_menuitem_relativeLayout);
		rlytTaphouseMenuItem = (RelativeLayout) findViewById(R.id.taphouse_menuitem_relativeLayout);
	}
	
	public void initValue() {
		txtUserName.setText(GlobalData.currentUser.firstName + " " + GlobalData.currentUser.lastName);
		imgProfile.setImageUrl("https://graph.facebook.com/" + GlobalData.currentUser.userToken + "/picture");
		txtCityName.setText(GlobalData.curCityName.toUpperCase());
		
//		Bitmap bmp = ImageUtility.getBitmapFromUrl("https://graph.facebook.com/" + GlobalData.currentUser.userToken + "/picture");
		
		String strLat = Double.toString(GlobalData.curLat);
		String strLng = Double.toString(GlobalData.curLng);
		
		Log.e("Latitude, Longitude = ", strLat + " " + strLng);
		
//		new MyAsyncTask(ResultActivity.this).execute(MyAsyncTask.ACTION_SEARCH, strLat, strLng, "", 
//				"", GlobalData.SEARCH_TYPE_CAB_IT);
		
    	lstAdvertise.setAdapter(new AdvertisesAdapter(this, R.layout.row_advertise, GlobalData.arrAdvertise));
    	llytSrchPubTypeMenuBar.setVisibility(View.GONE);
    	isOpenPubTypeMenu = false;
	}
	
	public void initEvent() {
		
		btnMenu.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (GlobalData.currentUser.userType.equals("1")) {
					ResultActivity.this.startActivity(new Intent(ResultActivity.this, PubPatronActivity.class));
//					ResultActivity.this.startActivity(new Intent(ResultActivity.this, PubOwnerActivity.class));
				}
				if (GlobalData.currentUser.userType.equals("2")) {
//					ResultActivity.this.startActivity(new Intent(ResultActivity.this, PubPatronActivity.class));
					ResultActivity.this.startActivity(new Intent(ResultActivity.this, PubOwnerActivity.class));


//					ResultActivity.this.startActivity(new Intent(ResultActivity.this, PubOwnerActivity.class));
				}
			}
        });
		
		rlytSrchPubTypeMenu.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				isOpenPubTypeMenu = !isOpenPubTypeMenu;
				
				if (isOpenPubTypeMenu) {
					initPubTypeMenuItem();
					llytSrchPubTypeMenuBar.setVisibility(View.VISIBLE);
				} else {
					llytSrchPubTypeMenuBar.setVisibility(View.GONE);
				}
			}
        });
		
		rlytWesternMenuItem.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				SearchActivity.strPubType = "1";
				hidePubTypeMenuBar();
			}
        });
		
		rlytSportMenuItem.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {	
				SearchActivity.strPubType = "2";
				hidePubTypeMenuBar();
			}
        });
		
		rlytIrishMenuItem.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {				
				SearchActivity.strPubType = "3";
				hidePubTypeMenuBar();
			}
        });
		
		rlytClassicMenuItem.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				SearchActivity.strPubType = "4";
				hidePubTypeMenuBar();
			}
        });
		
		rlytTaphouseMenuItem.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				SearchActivity.strPubType = "5";
				hidePubTypeMenuBar();
			}
        });
		
		llytSrchAllPubsButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				SearchActivity.strPubType = "";
				startActivity(new Intent(ResultActivity.this, FeatureActivity.class));
			}
        });
	}
	
	private void hidePubTypeMenuBar() {
		Timer timer = new Timer();
	    timer.schedule(new TimerTask() {
	           @Override
	           public void run() {
	        	   ResultActivity.this.runOnUiThread(new Runnable() {
	        		   public void run() {
	    	        	   llytSrchPubTypeMenuBar.setVisibility(View.GONE);
	    	        	   isOpenPubTypeMenu = false;
	        		   }
	        	   });
	           }
	    }, DELAY_TIME_HIDE_PUB_TYPE_MENUBAR);
	    
	    startActivity(new Intent(this, FeatureActivity.class));
	}
	
	public static void setUnreadMessageCount() {
		txtUnreadMessageCount.setText(Integer.toString(GlobalData.arrMyMessage.size()));
		
		Bitmap bmp = ImageUtility.getBmpFromImageView(imgProfile);
		bmp = ImageUtility.getRoundCornerBmp(bmp, 9);
		
		GlobalData.bmpProfile = bmp;
		imgProfile.setImageBitmap(bmp);
	}
	
	private void initPubTypeMenuItem() {
		
//		rlytWesternMenuItem.setBackgroundColor(0xe1feb902);
//		rlytSportMenuItem.setBackgroundColor(0xe1feb902);
//		rlytIrishMenuItem.setBackgroundColor(0xe1feb902);
//		rlytClassicMenuItem.setBackgroundColor(0xe1feb902);
//		rlytTaphouseMenuItem.setBackgroundColor(0xe1feb902);
	}
	
	public class AdvertisesAdapter extends ArrayAdapter<Advertise> {
    	
    	ArrayList<Advertise> arrData;
    	int row_id;

		public AdvertisesAdapter(Context context, int _row_id, ArrayList<Advertise> _arrData) {
			super(context, _row_id, _arrData);
			// TODO Auto-generated constructor stub
			row_id = _row_id;
			arrData = _arrData;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
	        View row = convertView;
	        LayoutInflater inflater = ResultActivity.this.getLayoutInflater();
	        row = inflater.inflate(row_id, parent, false);
	        
	        TextView txtAdTitle = (TextView) row.findViewById(R.id.row_name_textView);
	        TextView txtAdType = (TextView) row.findViewById(R.id.row_desc_textView);
	        SmartImageView imgAdIcon = (SmartImageView) row.findViewById(R.id.pub_owner_icon_imageView);
	        Button btnSee = (Button) row.findViewById(R.id.change_pub_owner_icon_button);
	        
	        btnSee.setId(SEE_BUTTON_INDEX + position);

	        Advertise advertise = arrData.get(position);

	        txtAdTitle.setText(advertise.title);

	        imgAdIcon.setImageUrl(advertise.adIcon);
	        txtAdTitle.setText(advertise.title);
	        txtAdType.setText(advertise.type);
	     
	        btnSee.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View v) {
					int pos = v.getId() - SEE_BUTTON_INDEX;
					Advertise ad = arrData.get(pos);
					Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(ad.link));
					ResultActivity.this.startActivity(intent);
				}
	        });
	        
//	        imgAdIcon.setImageUrl("http://www.edumobile.org/iphone/wp-content/uploads/2013/06/Screenshot_41.png");
	        
	        return row;
		}
    }
}
