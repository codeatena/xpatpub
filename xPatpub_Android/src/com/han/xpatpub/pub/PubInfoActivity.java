package com.han.xpatpub.pub;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.R;
import com.han.xpatpub.feature.AlesMenu;
import com.han.xpatpub.feature.TunesMenu;
import com.han.xpatpub.feature.VibeMenu;
import com.han.xpatpub.model.Pub;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class PubInfoActivity extends Activity {
	
	public Pub curPub = new Pub();
	public static int curPubID;
	
	Button btnBack;
	TextView txtPubName;
	
	ImageView imgSelVibe;
	ImageView imgSelTunes;
	
	ImageView imgSelGames;
	ImageView imgSelDarts;
	ImageView imgSelPool;
	ImageView imgSelCards;
	ImageView imgSelShuffleBoard;
	
	ImageView imgSelAles;
	
	TextView txtSelVibe;
	TextView txtSelTunes;
	TextView txtSelAles;

	Button btnCritique;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pub_info);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	public void initWidget() {
		btnBack = (Button) findViewById(R.id.pub_patron_back_button);
		txtPubName = (TextView) findViewById(R.id.pub_info_pub_name_textView);
		
		imgSelVibe = (ImageView) findViewById(R.id.vibe_sel_imageView);
		imgSelTunes = (ImageView) findViewById(R.id.tunes_sel_imageView);
		
		imgSelGames = (ImageView) findViewById(R.id.game_sel_imageView);
		imgSelDarts = (ImageView) findViewById(R.id.darts_sel_imageView);
		imgSelPool = (ImageView) findViewById(R.id.pool_sel_imageView);
		imgSelCards = (ImageView) findViewById(R.id.cards_sel_imageView);
		imgSelShuffleBoard = (ImageView) findViewById(R.id.shuffleboard_sel_imageView);
		
		imgSelAles = (ImageView) findViewById(R.id.ales_sel_imageView);
		
		txtSelVibe = (TextView) findViewById(R.id.vibe_sel_textView);
		txtSelTunes = (TextView) findViewById(R.id.tunes_sel_textView);
		txtSelAles = (TextView) findViewById(R.id.ales_sel_textView);
		
		btnCritique = (Button) findViewById(R.id.submit_button);
	}
	
	public void initValue() {
//		drawPub();

	}
	
	public void initEvent() {
		btnBack.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		btnCritique.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				CritiqueActivity.curPub = curPub;
				PubInfoActivity.this.startActivity(new Intent(PubInfoActivity.this, CritiqueActivity.class));
			}
		});
	}
	
	public void drawPub() {
		curPub = GlobalData.getPub(curPubID);
		txtPubName.setText(curPub.name);
		
		drawVibe();
		drawTunes();
		drawAction();
		drawAles();
	}
	
	private void drawVibe() {
		if (curPub.nVibe == VibeMenu.LAID_BACK) {
			imgSelVibe.setImageResource(R.drawable.laidback_sel);
			txtSelVibe.setText("Laidback");
		}
		if (curPub.nVibe == VibeMenu.JAZZY) {
			imgSelVibe.setImageResource(R.drawable.jazzy_sel);
			txtSelVibe.setText("Jazzy");
		}
		if (curPub.nVibe == VibeMenu.RAUNCHY) {
			imgSelVibe.setImageResource(R.drawable.raunchy_sel);
			txtSelVibe.setText("Raunchy");
		}
		if (curPub.nVibe == VibeMenu.MEAT_MARKET) {
			imgSelVibe.setImageResource(R.drawable.meat_market_sel);
			txtSelVibe.setText("Meet Market");
		}
		if (curPub.nVibe == Pub.NONE) {
			imgSelVibe.setImageResource(android.R.color.transparent);
			txtSelVibe.setText("");
		}
	}
	
	private void drawTunes() {
		if (curPub.nTunes == TunesMenu.LIVE) {
			imgSelTunes.setImageResource(R.drawable.live_sel);
			txtSelTunes.setText("Live");
		}
		if (curPub.nTunes == TunesMenu.CANNED) {
			imgSelTunes.setImageResource(R.drawable.canned_sel);
			txtSelTunes.setText("Canned");
		}
		if (curPub.nTunes == TunesMenu.BOTH) {
			imgSelTunes.setImageResource(R.drawable.both_sel);
			txtSelTunes.setText("Both");
		}
		if (curPub.nTunes == Pub.NONE) {
			imgSelTunes.setImageResource(android.R.color.transparent);
			txtSelTunes.setText("");
		}
	}

	private void drawAction() {
		
		imgSelGames.setVisibility(View.GONE);
		imgSelDarts.setVisibility(View.GONE);
		imgSelPool.setVisibility(View.GONE);
		imgSelCards.setVisibility(View.GONE);
		imgSelShuffleBoard.setVisibility(View.GONE);
		
		if (curPub.isGames) {
			imgSelGames.setVisibility(View.VISIBLE);
		}
		if (curPub.isDarts) {
			imgSelDarts.setVisibility(View.VISIBLE);
		}
		if (curPub.isPool) {
			imgSelPool.setVisibility(View.VISIBLE);
		}
		if (curPub.isCards) {
			imgSelCards.setVisibility(View.VISIBLE);
		}
		if (curPub.isShuffleboard) {
			imgSelShuffleBoard.setVisibility(View.VISIBLE);
		}
	}
	
	private void drawAles() {
		if (curPub.nAles == AlesMenu.LESS_FOUR) {
			imgSelAles.setImageResource(R.drawable.less_four_sel);
			txtSelAles.setText("Less than 4");
		}
		if (curPub.nAles == AlesMenu.FOUR_TO_TEN) {
			imgSelAles.setImageResource(R.drawable.four_to_ten_sel);
			txtSelAles.setText("4 to 10");
		}
		if (curPub.nAles == AlesMenu.MORE_TEN) {
			imgSelAles.setImageResource(R.drawable.more_ten_sel);
			txtSelAles.setText("More than 10");
		}
		if (curPub.nAles == Pub.NONE) {
			imgSelAles.setImageResource(android.R.color.transparent);
			txtSelAles.setText("");
		}
	}
	
	public void onResume() {
		super.onResume();
		
		Log.e("test", "Resume");
		drawPub();
	}
}
