package com.han.xpatpub.pub;

import com.han.xpatpub.GlobalData;
import com.han.xpatpub.R;
import com.han.xpatpub.addpub.ChoosePubFeature;
import com.han.xpatpub.model.Pub;
import com.han.xpatpub.network.MyAsyncTask;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class CritiqueActivity extends Activity {
	
	Button btnBack;
	Button btnSubmit;
	
	RelativeLayout rlytChoosePubFeature;
	public static Pub curPub = new Pub();
	
	ChoosePubFeature viewChoosePubFeature;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_critique);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	public void initWidget() {
		btnBack = (Button) findViewById(R.id.pub_patron_back_button);
		btnSubmit = (Button) findViewById(R.id.submit_button);
		
		rlytChoosePubFeature = (RelativeLayout) findViewById(R.id.choose_pub_feature_relativeLayout);
		viewChoosePubFeature = new ChoosePubFeature(this);
		rlytChoosePubFeature.addView(viewChoosePubFeature.view);
	}
	
	public void initValue() {

	}
	
	public void initEvent() {
		btnBack.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		btnSubmit.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				String strFeature = viewChoosePubFeature.getStrFeature();
				new MyAsyncTask(CritiqueActivity.this).execute(MyAsyncTask.ACTION_UPDATE_FEATURE, Integer.toString(curPub.id), strFeature);
			}
		});
	}
	
	public void setRate(int nRate) {

	}
	
	public void successAddRate() {
//		this.setResult(RESULT_OK);
		
		String strLat = Double.toString(GlobalData.curLat);
		String strLng = Double.toString(GlobalData.curLng);
		
		new MyAsyncTask(CritiqueActivity.this).execute(MyAsyncTask.ACTION_SEARCH_PUB, strLat, strLng, "", FeatureActivity.strFeature,
				Double.toString(GlobalData.MILES_FOR_CAB_IT));
	}
	
	public void successSearch() {
		this.finish();
	}
	
	public void successUpdateFeature() {
		String strLat = Double.toString(GlobalData.curLat);
		String strLng = Double.toString(GlobalData.curLng);
		
		new MyAsyncTask(CritiqueActivity.this).execute(MyAsyncTask.ACTION_SEARCH_PUB, strLat, strLng, "", FeatureActivity.strFeature,
				Double.toString(GlobalData.MILES_FOR_CAB_IT));
	}
}
